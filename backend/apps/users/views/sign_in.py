from django.utils import timezone
from django.conf import settings
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from users.models import User, SmsCode, Token
from users.serializers.user import UserSerializer
from users.utils.send_code import send_code
from users.utils.validation_phone_number import validation_phone_number


class SignInView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        user_id = request.data.get('user_id')
        code = request.data.get('verification_code')

        if not user_id or not code:
            return Response({
                'detail': '`user_id` и `verification_code` обязательные поля!'
            }, 400)
        sms_code = SmsCode.objects.filter(user_id=user_id).last()

        if sms_code.code != code:
            return Response({
                'detail': 'Incorrect sms verification code!'
            }, 400)

        user = User.objects.filter(id=user_id).first()
        user.last_login = timezone.now()
        user.is_registered = True
        user.save()

        token, _ = Token.objects.get_or_create(user_id=user.id, expires_at__gte=timezone.now())
        user_data = UserSerializer(user).data

        return Response({"user": user_data, "token": token.key}, 201)


class SendVerificationCodeView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        if not request.data.get('phone') or not validation_phone_number(request.data.get("phone")):
            return Response({'detail': 'Неправильно предоставлен номер телефона!'}, 400)

        user, _ = User.objects.get_or_create(phone=request.data.get('phone'), defaults={
            'username': request.data.get('phone'),
        })

        if settings.SMS_CODE_ACTIVE:
            res = send_code(user)
            return Response(res, 201)

        SmsCode.objects.get_or_create(user_id=user.id, code='00000')
        # WITHOUT ESKIZ API DEFAULT CODE is 0000, but will work when ran `python manage.py  fixtures`
        fake_data = {
            "message_status": {
                "status": "success",
                "message": "Waiting for SMS provider"
            },
            "user_id": user.id
        }
        return Response(fake_data, 201)
