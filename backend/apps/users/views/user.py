from rest_framework.generics import GenericAPIView, get_object_or_404
from rest_framework.response import Response

from users.models import User
from users.serializers.user import UserDetailSerializer, UserSimpleSerializer


class UserDetailView(GenericAPIView):
    def get(self, request, pk):
        instance = get_object_or_404(User, id=pk)
        serializer = UserSimpleSerializer(instance).data
        return Response(serializer)

    def put(self, request, pk):
        instance = get_object_or_404(User, id=pk)
        serializer = UserDetailSerializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=request.user)
        return Response(serializer.data)
