from django.urls import path
from users.views.sign_in import SignInView, SendVerificationCodeView
from users.views.user import UserDetailView
from users.views.client import ClientAPIView, ClientDetailAPIView

urlpatterns = [
    path('sign-in/', SignInView.as_view(), name='sign-in'),
    path('send-verification-code/', SendVerificationCodeView.as_view(), name='send-verification-code'),

    path('user/<int:pk>', UserDetailView.as_view(), name='user-detail'),
    path('client', ClientAPIView.as_view(), name='client'),
    path('clients/<int:pk>', ClientDetailAPIView.as_view(), name='client-detail'),
]
