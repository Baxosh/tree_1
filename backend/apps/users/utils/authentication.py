from django.utils import timezone
from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import AuthenticationFailed

from users.models import Token


class CustomTokenAuthentication(TokenAuthentication):
    def authenticate_credentials(self, key):
        token = Token.objects.select_related('user').filter(key=key, expires_at__gte=timezone.now()).first()

        if token is None:
            raise AuthenticationFailed('Invalid or expired token.')

        if not token.user.is_active:
            raise AuthenticationFailed('User inactive or deleted.')

        if not token.is_active:
            raise AuthenticationFailed('Your token is not active.')

        return token.user, token
