from django.db.models import CASCADE

from django.db import models
from django.contrib.auth.models import AbstractUser

from core.utils.files import file_path
from main.models import BaseModel
from users.queryset.sms_code import SmsCodeManager
from users.queryset.client import ClientManager
from users.queryset.donator import DonatorManager
from users.utils import tokens
from users.utils.fields import expires_default

ANDIJON = "ANDIJON"
BUXORO = "BUXORO"
FARGONA = "FARGONA"
NAMANGAN = "NAMANGAN"
JIZZAX = "JIZZAX"
SIRDARYO = "SIRDARYO"
SAMARQAND = "SAMARQAND"
NANOIY = "NANOIY"
QASHQADARYO = "QASHQADARYO"
SURXONDARYO = "SURXONDARYO"
XORAZM = "XORAZM"
QORAQALPOGISTON = "QORAQALPOGISTON"
TOSHKENT_VILOYAT = "TOSHKENT_VILOYAT"
TOSHKENT_SHAXAR = "TOSHKENT_SHAXAR"

REGIONS = (
    (ANDIJON, "Andhijan"),
    (BUXORO, "Bukhara"),
    (FARGONA, "Fergana"),
    (NAMANGAN, "Namangan"),
    (JIZZAX, "Jizzakh"),
    (SIRDARYO, "Sirdaryo"),
    (SAMARQAND, "Samarkand"),
    (NANOIY, "Navoiy"),
    (QASHQADARYO, "Kashkadarya"),
    (SURXONDARYO, "Surxondaryo"),
    (XORAZM, "Khorezm"),
    (QORAQALPOGISTON, "Karakalpakstan"),
    (TOSHKENT_VILOYAT, "Tashkent Region"),
    (TOSHKENT_SHAXAR, "Tashkent City"),
)

# REGIONS = [(english_name, uzbek_name) for uzbek_name, english_name in REGION_TRANSLATIONS.items()]


class User(AbstractUser, BaseModel):
    phone = models.CharField(max_length=15, unique=True)
    name = models.CharField(max_length=255, null=True, blank=True)
    photo = models.ImageField(upload_to=file_path('users'), null=True, blank=True)
    region = models.CharField(max_length=250, choices=REGIONS, default=TOSHKENT_SHAXAR)
    is_manager = models.BooleanField(default=False)
    is_registered = models.BooleanField(default=False)

    class Meta(AbstractUser.Meta):
        db_table = 'users_user'

    def __str__(self):
        return str(self.id)


class SmsCode(BaseModel):
    code = models.CharField(max_length=5)
    user = models.ForeignKey('users.User', CASCADE, 'sms_codes')

    objects = SmsCodeManager.as_manager()

    def __str__(self):
        return self.code

    class Meta:
        db_table = 'users_sms_code'


class Donator(BaseModel):
    donated = models.PositiveIntegerField(default=0)
    description = models.TextField()

    user = models.ForeignKey('users.User', CASCADE, 'donators')

    objects = DonatorManager.as_manager()

    class Meta(AbstractUser.Meta):
        db_table = 'users_donator'

    def __str__(self):
        return str(self.id)


class Client(BaseModel):
    balance = models.PositiveIntegerField(default=0)
    user = models.OneToOneField('users.User', CASCADE)

    objects = ClientManager.as_manager()

    class Meta(AbstractUser.Meta):
        db_table = 'users_client'

    def __str__(self):
        return str(self.id)


class Token(BaseModel):
    key = models.CharField(max_length=40, unique=True)
    is_active = models.BooleanField(default=True)
    user = models.ForeignKey(User, models.CASCADE, related_name='tokens')
    expires_at = models.DateTimeField(default=expires_default)  # token expires in 30 days

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = tokens.generate()
        return super(Token, self).save(*args, **kwargs)

    def __str__(self):
        return self.key

    class Meta:
        db_table = 'users_token'
