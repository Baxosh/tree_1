from rest_framework import serializers
from users.models import User, REGIONS, Client, Donator
from users.serializers.client import ClientSerializer


class UserSimpleSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        data = super().to_representation(instance)
        try:
            data['balance'] = instance.client.balance if instance.client else None
        except Exception:
            data['balance'] = 0

        return data

    class Meta:
        model = User
        fields = ('id', 'name', 'phone', 'photo', 'region', 'is_manager')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'name', 'phone', 'photo', 'region', 'is_manager')


class UserDetailSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=255, required=True)
    role = serializers.ChoiceField(choices=['donator', 'client'], required=True)
    region = serializers.ChoiceField(choices=REGIONS, required=True)
    phone = serializers.CharField(max_length=255, required=False, read_only=True)

    def update(self, instance, data):
        instance = super().update(instance, data)
        role = data.get('role')
        if role == 'client':
            Client.objects.get_or_create(user_id=instance.id)

        if role == 'donator':
            Donator.objects.get_or_create(user_id=instance.id)
        return instance

    class Meta:
        model = User
        fields = ('id', 'name', 'phone', 'photo', 'region', 'is_manager', 'role')
