from rest_framework.views import APIView
from main.serizlizers.donator import DonatorSerializer, DonatorsSerializer
from users.models import Donator
from rest_framework.response import Response
from main.serizlizers.order import OrderDonatorSerializer
from main.models import Order


class DonatorBalance(APIView):
    def get(self, request):
        donators = Donator.objects.select_related('user').all()
        serializer = DonatorSerializer(instance=donators, many=True).data
        return Response(serializer, status=200)


class DonatorListView(APIView):
    def get(self, request, *args, **kwargs):
        donators = Donator.objects.select_related('user').order_by('-donated')
        serializer = DonatorsSerializer(donators, many=True)
        return Response(serializer.data)


class DonatorOrdersView(APIView):
    def get(self, request, donator_id, *args, **kwargs):
        orders = Order.objects.filter(donator__id=donator_id)
        serializer = OrderDonatorSerializer(orders, many=True)
        return Response(serializer.data)