from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from rest_framework.views import APIView
from main.serizlizers.order import OrderCreateSerializer
from main.models import Order


class OrderCreateAPIView(generics.CreateAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderCreateSerializer


class ComplateOrderView(APIView):
    def post(self, request, order_id, format=None):
        try:
            order = Order.objects.get(id=order_id)
        except Order.DoesNotExist:
            return Response({'error': "Order Not Found"}, status=status.HTTP_400_BAD_REQUEST)

        if order.status == Order.IN_PROCESS:
            order.status = Order.COMPLETED
            order.save()

            if order.client:
                order.client.balance += int(order.price)
            return Response({'message': 'Order completed and balance update'}, status=status.HTTP_200_OK)
        else:
            return Response({'error': 'Order is not  in process'}, status=status.HTTP_400_BAD_REQUEST)

