from django.db.models import Prefetch

from main.serizlizers.plant import PlantFilterSerializer, PlantSerializer, PlanCreateSerializer
from main.serizlizers.order import OrderSerializer, OrdersSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from main.models import Plant, Order
from users.models import Client, Donator
from rest_framework import status
from rest_framework import generics


class PlantListApiView(APIView):
    def get(self, request):
        params = PlantFilterSerializer.check(request.GET)
        plants = Plant.objects.list(
            region=params.get('region'),
            is_completed=params.get('is_completed'),
            is_confirmed=params.get('is_confirmed'),
        )
        serializers = PlantSerializer(plants, many=True)
        return Response(serializers.data)

    def post(self, request):
        serializers = PlanCreateSerializer(context={'request': request}, data=request.data)
        serializers.is_valid(raise_exception=True)
        serializers.save()
        return Response(serializers.data, 201)


class ClientSelectOrderView(APIView):
    def get(self, request, order_id):
        try:
            order = Order.objects.get(pk=order_id)
            serializer = OrderSerializer(order)
            return Response(serializer.data)
        except Order.DoesNotExist:
            return Response({"message": "Order not found"}, status=404)


class AcceptOrderView(APIView):
    def put(self, request, order_id):
        try:
            client = Client.objects.get(pk=request.user.id)
            order = Order.objects.get(pk=order_id)
            if order.status == Order.WAITING:
                order.client = client
                order.status = Order.IN_PROCESS
                order.save()
                return Response({"message": "Order accepted and moved to IN_PROCESS status"}, status=status.HTTP_200_OK)
            else:
                return Response({"message": "Order cannot be accepted in its current status"},
                                status=status.HTTP_400_BAD_REQUEST)
        except Order.DoesNotExist:
            return Response({"message": "Order not found"}, status=status.HTTP_404_NOT_FOUND)


class OrderDetailView(APIView):
    def get(self, request, pk):
        queryset = Order.objects.filter(donator=pk)
        serializer = OrdersSerializer(instance=queryset, many=True).data
        return Response(serializer)

