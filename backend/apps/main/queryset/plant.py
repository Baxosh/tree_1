from main.queryset.base_queryset import BaseQuerySet


class PlantQuerySet(BaseQuerySet):
    def list(self, is_confirmed=None, is_completed=None, region=None):
        query = self.filter(region=region) if region else self
        query = query.filter(is_completed=is_completed) if is_completed is not None else query
        query = query.filter(is_confirmed=is_confirmed) if is_confirmed is not None else query
        return query
