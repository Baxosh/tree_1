from django.db import models
from core.models import BaseModel
from main.queryset.plant import PlantQuerySet
from main.queryset.order import OrderQuerySet
from django.db.models import CASCADE

from users.models import TOSHKENT_SHAXAR, REGIONS


class Plant(BaseModel):
    TREE_TYPE = (
        ("KASHTAN", "Kashtan"),
        ("CHINOR", "Akatsiya"),
        ("AKATSIYA", "Akatsiya"),
        ("OQ QAYIN", "Oq qayin"),
        ("MAJNUNTOL", "Majnuntol"),
        ("SARV", "Sarv"),
        ("TERAK", "Terak"),
        ("SHAMSHOD", "Shamshod"),
        ("DO'LANA", "Do'lana"),
        ("ARCHA", "Archa"),
        ("OQ QARAG'AY", "Oq qarag'ay"),
    )

    a = models.CharField(max_length=255)
    b = models.CharField(max_length=255)
    c = models.CharField(max_length=255)
    d = models.CharField(max_length=255)
    planted = models.PositiveIntegerField(default=0)
    count = models.PositiveIntegerField(default=1)
    is_confirmed = models.BooleanField(default=False)
    is_completed = models.BooleanField(default=False)
    region = models.CharField(max_length=255, choices=REGIONS, default=TOSHKENT_SHAXAR)
    tree_type = models.CharField(max_length=255, choices=TREE_TYPE, default='KASHTAN')

    objects = PlantQuerySet.as_manager()

    class Meta:
        db_table = 'main_plant'

    def __str__(self):
        return str(self.count)


class Order(BaseModel):
    WAITING = "Kutilmoqda"
    PAID = "To'langan"
    IN_PROCESS = "Ish jarayonida"
    COMPLETED = "Tugatilgan"

    TYPES = (
        (WAITING, 'Kutilmoqda'),
        (PAID, "To'langan"),
        (IN_PROCESS, 'Ish jarayonida'),
        (COMPLETED, 'Tugatilgan'),
    )

    count = models.PositiveIntegerField(default=1)
    status = models.CharField(max_length=255, choices=TYPES, default=WAITING)
    donator = models.ForeignKey('users.Donator', CASCADE, 'orders')
    client = models.ForeignKey('users.Client', CASCADE, 'orders', null=True, blank=True)
    plant = models.ForeignKey('main.Plant', CASCADE, 'orders')
    accepted_at = models.DateTimeField(null=True, blank=True)
    completed_at = models.DateTimeField(null=True, blank=True)
    price = models.CharField(max_length=255)

    objects = OrderQuerySet.as_manager()

    class Meta:
        db_table = 'main_order'

    def __str__(self):
        return f'{self.donator}, {self.count}'
