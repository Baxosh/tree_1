from django.urls import path
from main.views.plant import PlantListApiView, ClientSelectOrderView, AcceptOrderView, OrderDetailView
from main.views.order import OrderCreateAPIView, ComplateOrderView
from main.views.donator import DonatorBalance, DonatorListView, DonatorOrdersView

urlpatterns = [
    path('plant/', PlantListApiView.as_view(), name='plant-list'),
    path('order-client/<int:order_id>/', ClientSelectOrderView.as_view(), name="order-list"),
    # path('order/complate/<int:order_id>/', CompleteOrderView.as_view(), name='complete_order'),
    path('order/<int:pk>', OrderDetailView.as_view(), name='orders'),
    path('order-create/', OrderCreateAPIView.as_view(), name="order-create"),
    path('complate-order/<int:order_id>/', ComplateOrderView.as_view(), name='complete-order'),
    path('Accept-order/<int:order_id>/', AcceptOrderView.as_view(), name="accept-order"),
    path('donator/', DonatorBalance.as_view(), name='donator'),
    path('donators/', DonatorListView.as_view(), name='donators'),
    path('donators/<int:donator_id>/orders/', DonatorOrdersView.as_view(), name='donator-order'),
]
