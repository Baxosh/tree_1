from rest_framework import serializers
from main.models import Order
from main.serizlizers.plant import PlantSerializers


class OrderSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['plant'] = PlantSerializers(instance.plant).data
        return data

    class Meta:
        model = Order
        fields = ('count', 'status', 'donator', 'client', 'plant', 'price')


class OrdersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ('count', 'status', 'donator', 'client', 'plant', 'price')


class OrderCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ('count', 'donator', 'plant', 'price')


class OrderDonatorSerializer(serializers.ModelSerializer):
    donator_id = serializers.IntegerField(source='donator.id', read_only=True)
    donator_name = serializers.CharField(source='donator.user.name', read_only=True)
    donator_phone = serializers.CharField(source='donator.user.phone', read_only=True)
    donator_photo = serializers.ImageField(source='donator.user.photo', read_only=True)
    donator_region = serializers.CharField(source='donator.user.region', read_only=True)

    class Meta:
        model = Order
        fields = '__all__'
