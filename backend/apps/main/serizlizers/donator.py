from rest_framework import serializers
from users.models import Donator
from users.models import User
from users.serializers.user import UserSerializer


class DonatorSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['user'] = UserSerializer(instance.user).data
        return data

    class Meta:
        model = Donator
        fields = ('id', 'user', 'donated')


class UserDonatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'name')

    donator = DonatorSerializer()


class DonatorsSerializer(serializers.ModelSerializer):
    user_name = serializers.CharField(source='user.name', read_only=True)
    user_phone = serializers.CharField(source='user.phone', read_only=True)
    user_photo = serializers.ImageField(source='user.photo', read_only=True)
    user_region = serializers.CharField(source='user.region', read_only=True)
    is_manager = serializers.BooleanField(source='user.is_manager', read_only=True)
    is_registered = serializers.BooleanField(source='user.is_registered', read_only=True)

    class Meta:
        model = Donator
        fields = '__all__'


