from geopy import Nominatim
from rest_framework import serializers

from core.utils.serializers import ValidatorSerializer
from main.models import Plant, Order
from users.models import REGIONS, Donator


class PlantSerializers(serializers.ModelSerializer):
    class Meta:
        model = Plant
        fields = ('a', 'b', 'c', 'd', 'planted', 'count', 'region')


address = serializers.SerializerMethodField('get_address')


class PlantSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['a'] = data.get('a').split(';')
        data['b'] = data.get('b').split(';')
        data['c'] = data.get('c').split(';')
        data['d'] = data.get('d').split(';')
        data['address'] = Nominatim(user_agent="Mirzohid").reverse((data['a'][0], data['a'][1]),
                                                                   exactly_one=True).address
        return data

    class Meta:
        model = Plant
        fields = (
            'id',
            'a',
            'b',
            'c',
            'd',
            'planted',
            'count',
            'is_confirmed',
            'is_completed',
            'region',
        )


class PlantFilterSerializer(ValidatorSerializer):
    region = serializers.ChoiceField(choices=REGIONS, required=False)
    is_confirmed = serializers.ChoiceField(choices=[1, 0], required=False)
    is_completed = serializers.ChoiceField(choices=[1, 0], required=False)


class PlanCreateSerializer(serializers.ModelSerializer):
    a = serializers.CharField(max_length=255)
    b = serializers.CharField(max_length=255)
    c = serializers.CharField(max_length=255)
    d = serializers.CharField(max_length=255)
    count = serializers.IntegerField(min_value=1, required=True)
    region = serializers.ChoiceField(choices=REGIONS, required=True)
    tree_type = serializers.ChoiceField(choices=Plant.TREE_TYPE, required=True)

    def create(self, data):
        instance = super().create(data)
        user = self.context['request'].user
        order = Order(donator=Donator.objects.filter(user=user).first(), plant=instance, count=instance.count)
        order.save()
        return instance

    class Meta:
        model = Plant
        fields = ('id', 'a', 'b', 'c', 'd', 'count', 'region', 'tree_type')
