from django.contrib import admin
from finance.models import Card, Payment
from main.models import Plant, Order
from users.models import User, Donator, Client, SmsCode, Token


class AuthorMixin:
    def save_model(self, request, obj, form, change):
        if change:
            obj.updated_by = request.user
        else:
            obj.created_by = request.user

        super().save_model(request, obj, form, change)


@admin.register(Card)
class CardAdmin(AuthorMixin, admin.ModelAdmin):
    list_display = ('user', 'number', 'due_date')


@admin.register(Payment)
class PaymentAdmin(AuthorMixin, admin.ModelAdmin):
    list_display = ('user', 'card', 'count', 'amount')

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)

        if change:
            return

        Plant.objects.bulk_create([
            Plant(type='oak', investor=obj.user, payment=obj)
            for _ in range(obj.count)
        ])

    # def has_change_permission(self, request, obj=None):
    #     return not settings.SMS_CODE_ACTIVE
    #
    # def has_delete_permission(self, request, obj=None):
    #     return not settings.SMS_CODE_ACTIVE


@admin.register(Plant)
class PlantAdmin(AuthorMixin, admin.ModelAdmin):
    list_display = ('region', 'is_confirmed', 'is_completed', 'planted')
    list_filter = ('is_confirmed', 'is_completed')


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('donator', 'client', 'status', 'plant', 'count')


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('name', 'region', 'is_manager', 'is_registered')


@admin.register(Donator)
class DonatorAdmin(admin.ModelAdmin):
    list_display = ('user', 'donated')


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('user', 'balance')


@admin.register(Token)
class TokenAdmin(AuthorMixin, admin.ModelAdmin):
    list_display = ('user', 'key', 'is_active', 'expires_at')


@admin.register(SmsCode)
class SmsCodeAdmin(AuthorMixin, admin.ModelAdmin):
    list_display = ('user',)
    fields = ('dispatch_id', 'code')

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
