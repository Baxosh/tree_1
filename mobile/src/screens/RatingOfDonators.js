import React, {useState} from 'react';
import {Text, View, Image, ScrollView, TouchableOpacity, Button, Pressable} from "react-native";
import {useFocusEffect, useNavigation} from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import BackButton from "../components/BackButton";
import MaskInput from "react-native-mask-input/src/MaskInput";
import {DONATOR_RATING, PLANT} from "../utils/urls";
import axios from "axios";

const RatingOfDonators = () => {
    const [donators, setDonators] = useState(null)

    const getDonatorsTop = async () => {
        try {
            const token = await AsyncStorage.getItem('token')
            const response = await axios.get(DONATOR_RATING, {
                headers: {
                    Authorization: `Token ${token}`
                }
            })
            setDonators(response.data)
        } catch (error) {
            console.log(error.response)
        }
    }

    useFocusEffect(
        React.useCallback(() => {
            getDonatorsTop()
        }, []),)

    return (
        <ScrollView className="flex-1 bg-gray-200">
            <BackButton/>
            <View className="items-center p-2">
                <Text className="text-[26px] mt-4 text-gray-500">Rating of All Investor</Text>

                <View className="w-full gap-3 p-5">
                    <Text className="text-[28px] mb-2 text-center font-semibold">Top 10 donators</Text>

                    {donators ? donators.map((donator, index) => (
                        <View
                            key={donator.id}
                            className="flex-row rounded-xl items-center p-3 bg-white"
                        >
                            <Text className="text-[22px] text-center mr-2">
                                {index + 1}
                            </Text>
                            <Text className="text-[22px] text-center">
                                {donator.user.name || '+' + donator.user.phone}
                            </Text>
                            <View className="ml-auto">
                                <Text className="text-[16px] text-center text-green-500">
                                    Donated:
                                </Text>
                                <Text className="text-[20px] text-center font-semibold text-gray-900">
                                    ${donator.donated}
                                </Text>
                            </View>
                        </View>
                    )): null}
                </View>
            </View>
        </ScrollView>
    );
};

export default RatingOfDonators;
