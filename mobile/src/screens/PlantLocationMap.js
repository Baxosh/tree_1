import React, {useEffect, useState} from 'react';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import {View, Dimensions, TouchableOpacity} from 'react-native';
import {useFocusEffect, useNavigation, useRoute} from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import BackButton from "../components/BackButton";
import {PLANT} from "../utils/urls";
import {averageData} from "../utils/getLocation";
import {G, Path, Svg} from "react-native-svg";

export default function PlantLocationMap() {
    const [locations, setLocations] = useState([])
    const [markLocations, setMarkLocations] = useState(null)
    const navigation = useNavigation()
    const route = useRoute();
    const region = route.params?.region

    const {width, height} = Dimensions.get("window")

    const ASPECT_RATIO = width / height;
    const LATITUDE_DELTA = 0.1;
    const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
    const INITIAL_POSITION = {
        latitude: 39.77472,
        longitude: 64.42861,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
    };

    const getLocationPlant = async () => {
        try {
            const token = await AsyncStorage.getItem('token')
            const response = await axios.get(PLANT, {
                params: {
                    region
                }, headers: {
                    Authorization: `Token ${token}`
                }
            })
            setLocations(response.data)
        } catch (error) {
            console.log(error.response.data.detail)
        }
    }

    useFocusEffect(React.useCallback(() => {
        getLocationPlant()
    }, []),)

    useEffect(() => {
        setMarkLocations(averageData(locations))
    }, [locations])

    const filterRegions = (id) => {
        let res = {}
        for (let item of locations) {
            if (id === item.id) {
                res = {...res, item}
            }
        }
        navigation.navigate('ConfirmOrder', {locationResult: res})
    }

    return (
        <View className='flex-1'>
            <BackButton/>
            <MapView
                className='w-full h-full'
                provider={PROVIDER_GOOGLE}
                initialRegion={INITIAL_POSITION}
                mapType={'standard'} // satellite
            >
                {markLocations && markLocations.map((location) => (
                    <Marker
                        onPress={() => filterRegions(location[0])}
                        key={location[0]}
                        coordinate={{
                            latitude: location[1],
                            longitude: location[2]
                        }}
                        className='items-center justify-center z-0'
                    >
                        <TouchableOpacity
                            className='p-1 rounded-2xl mb-2 relative'>
                            <Svg
                                xmlns="http://www.w3.org/2000/svg"
                                width={48}
                                height={48}
                                viewBox="0 0 256 256"
                            >
                                <G
                                    stroke="none"
                                    strokeWidth={0}
                                    strokeDasharray="none"
                                    strokeLinecap="butt"
                                    strokeLinejoin="miter"
                                    strokeMiterlimit={10}
                                    fill="none"
                                    fillRule="nonzero"
                                    opacity={1}
                                >
                                    <Path
                                        d="M45 90a4 4 0 01-3.444-1.966l-4.385-7.417C28.167 65.396 19.664 51.02 16.759 45.189a31.13 31.13 0 01-3.175-13.773C13.584 14.093 27.677 0 45 0s31.416 14.093 31.416 31.416c0 4.815-1.063 9.438-3.157 13.741-.025.052-.053.104-.08.155-2.961 5.909-11.41 20.193-20.353 35.309l-4.382 7.413A4 4 0 0145 90z"
                                        transform="matrix(2.81 0 0 2.81 1.407 1.407)"
                                        stroke="none"
                                        strokeWidth={1}
                                        strokeDasharray="none"
                                        strokeLinecap="butt"
                                        strokeLinejoin="miter"
                                        strokeMiterlimit={10}
                                        fill="#37b03f"
                                        fillRule="nonzero"
                                        opacity={1}
                                    />
                                    <Path
                                        d="M45 45.678c-8.474 0-15.369-6.894-15.369-15.368S36.526 14.941 45 14.941s15.368 6.895 15.368 15.369S53.474 45.678 45 45.678z"
                                        transform="matrix(2.81 0 0 2.81 1.407 1.407)"
                                        stroke="none"
                                        strokeWidth={1}
                                        strokeDasharray="none"
                                        strokeLinecap="butt"
                                        strokeLinejoin="miter"
                                        strokeMiterlimit={10}
                                        fill="#fff"
                                        fillRule="nonzero"
                                        opacity={1}
                                    />
                                </G>
                            </Svg>
                        </TouchableOpacity>
                    </Marker>
                ))}
            </MapView>
        </View>
    );
}