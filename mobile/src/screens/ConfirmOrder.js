import React, {useState, useEffect} from 'react';
import {Text, View, ScrollView, TouchableOpacity, Dimensions, TextInput, Linking} from "react-native";
import {useNavigation, useRoute} from "@react-navigation/native";
import BackButton from "../components/BackButton";
import MapView, {Marker, PROVIDER_GOOGLE} from "react-native-maps";
import DropDownPicker from "react-native-dropdown-picker";
import axios from "axios";
import {PLANT} from "../utils/urls";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {getAverageOfTwoNumbers} from "../utils/getLocation";
import {G, Path, Svg} from "react-native-svg";

const treeTypes = [
    {id: '1', label: "KASHTAN", value: "Kashtan"},
    {id: '2', label: "CHINOR", value: "Akatsiya"},
    {id: '3', label: "AKATSIYA", value: "Akatsiya"},
    {id: '4', label: "OQ_QAYIN", value: "Oq_qayin"},
    {id: '5', label: "MAJNUNTOL", value: "Majnuntol"},
    {id: '6', label: "SARV", value: "Sarv"},
    {id: '7', label: "TERAK", value: "Terak"},
    {id: '8', label: "SHAMSHOD", value: "Shamshod"},
    {id: '9', label: "DO'LANA", value: "Do'lana"},
    {id: '10', label: "ARCHA", value: "Archa"},
    {id: '11', label: "OQ_QARAG'AY", value: "Oq_qarag'ay"}
]

const ConfirmOrder = () => {
    const [open, setOpen] = useState(false);
    const [value, setValue] = useState('');
    const [items, setItems] = useState(treeTypes);
    const [count, setCount] = useState(null)
    const [location, setLocation] = useState()

    const route = useRoute()
    const resultLocation = route.params?.locationResult?.item

    useEffect(() => {
        resultLocation && setLocation(getAverageOfTwoNumbers(resultLocation))
    }, [resultLocation])

    const navigation = useNavigation()
    const {width, height} = Dimensions.get("window")

    const ASPECT_RATIO = width / height;
    const LATITUDE_DELTA = 0.01;
    const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
    const INITIAL_POSITION = {
        latitude: Number(resultLocation.a[0]),
        longitude: Number(resultLocation.a[1]),
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
    };
    const createPlant = async () => {
        try {
            const token = await AsyncStorage.getItem('token')
            const response = await axios.post(PLANT, {
                a: `${resultLocation.a[0]};${resultLocation.a[1]}`,
                b: `${resultLocation.b[0]};${resultLocation.b[1]}`,
                c: `${resultLocation.c[0]};${resultLocation.c[1]}`,
                d: `${resultLocation.d[0]};${resultLocation.d[1]}`,
                count: count,
                region: resultLocation.region,
                tree_type: value.toUpperCase()
            }, {
                headers: {
                    Authorization: `Token ${token}`
                }
            })
        } catch (error) {
            console.log(error.response)
        }
    }

    const webUri = 'https://www.apelsin.uz/open-service?serviceId=498611455'

    const openWebsite = () => {
        Linking.openURL(webUri).catch((err) => console.error('Не удалось открыть ссылку: ', err));
    };
    return (
        <View className="flex-1 min-h-screen bg-gray-200 p-5 items-center">
            <BackButton/>
            <Text className="text-[26px] font-semibold text-center mb-8 text-gray-500">Area name</Text>
            <View className="flex-1 mb-10 w-full">
                <View className='w-full h-32 rounded-xl overflow-hidden mb-4'>
                    <MapView
                        className='w-full h-full'
                        provider={PROVIDER_GOOGLE}
                        initialRegion={INITIAL_POSITION}
                        mapType={'standard'} // satellite
                    >
                        {location ? <Marker
                            coordinate={{
                                latitude: location[0],
                                longitude: location[1]
                            }}
                            className='items-center justify-center z-0'
                        >
                            <TouchableOpacity
                                className='p-1 rounded-2xl mb-2 relative'>
                                <Svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width={48}
                                    height={48}
                                    viewBox="0 0 256 256"
                                >
                                    <G
                                        stroke="none"
                                        strokeWidth={0}
                                        strokeDasharray="none"
                                        strokeLinecap="butt"
                                        strokeLinejoin="miter"
                                        strokeMiterlimit={10}
                                        fill="none"
                                        fillRule="nonzero"
                                        opacity={1}
                                    >
                                        <Path
                                            d="M45 90a4 4 0 01-3.444-1.966l-4.385-7.417C28.167 65.396 19.664 51.02 16.759 45.189a31.13 31.13 0 01-3.175-13.773C13.584 14.093 27.677 0 45 0s31.416 14.093 31.416 31.416c0 4.815-1.063 9.438-3.157 13.741-.025.052-.053.104-.08.155-2.961 5.909-11.41 20.193-20.353 35.309l-4.382 7.413A4 4 0 0145 90z"
                                            transform="matrix(2.81 0 0 2.81 1.407 1.407)"
                                            stroke="none"
                                            strokeWidth={1}
                                            strokeDasharray="none"
                                            strokeLinecap="butt"
                                            strokeLinejoin="miter"
                                            strokeMiterlimit={10}
                                            fill="#37b03f"
                                            fillRule="nonzero"
                                            opacity={1}
                                        />
                                        <Path
                                            d="M45 45.678c-8.474 0-15.369-6.894-15.369-15.368S36.526 14.941 45 14.941s15.368 6.895 15.368 15.369S53.474 45.678 45 45.678z"
                                            transform="matrix(2.81 0 0 2.81 1.407 1.407)"
                                            stroke="none"
                                            strokeWidth={1}
                                            strokeDasharray="none"
                                            strokeLinecap="butt"
                                            strokeLinejoin="miter"
                                            strokeMiterlimit={10}
                                            fill="#fff"
                                            fillRule="nonzero"
                                            opacity={1}
                                        />
                                    </G>
                                </Svg>
                            </TouchableOpacity>
                        </Marker> : null}
                    </MapView>
                </View>
                <View className="w-full flex-row justify-between">
                    <View className="w-[48%] px-4 py-2 bg-white rounded-xl mb-4">
                        <Text className="text-[18px] font-semibold text-gray-500">Available to plant</Text>
                        <Text className="text-[32px] font-semibold text-gray-500">{resultLocation.count}</Text>
                    </View>
                    <View className="w-[48%] px-2 py-2 bg-white rounded-xl mb-4">
                        <Text className="text-[16px] font-semibold text-gray-500">{resultLocation.address}</Text>
                    </View>
                </View>

                <DropDownPicker
                    placeholder="Select tree type"
                    className='w-full border-0 px-3 py-4 rounded-xl bg-white text-gray-700 mb-4'
                    dropDownContainerStyle={{
                        borderWidth: 0
                    }}
                    textStyle={{fontSize: 18, fontWeight: '500', color: '#3d3c3c'}}
                    open={open}
                    value={value}
                    items={items}
                    setOpen={setOpen}
                    setValue={setValue}
                    setItems={setItems}
                    // onChangeValue={(value) => user.region = value}
                />

                <TextInput
                    className='w-full border-0 px-3 py-4 rounded-xl bg-white text-gray-700 text-[18px] mb-6'
                    onChangeText={setCount}
                    value={count}
                    placeholder='How many trees?'
                    keyboardType='numeric'
                />
                <Text
                    className='w-full border-0 px-3 py-6 rounded-xl bg-white text-gray-700 text-[18px] mb-6'>$ {Number(count) * 5}</Text>
            </View>

            <TouchableOpacity
                disabled={!(count && value)}
                onPress={() => {
                    createPlant()
                    openWebsite()
                    // navigation.navigate('Transaction')
                }}
                className="absolute bottom-5 w-full px-4 py-3 bg-white rounded-xl mb-4">
                <Text className="text-[22px] font-semibold text-gray-500 text-center">Confirm</Text>
            </TouchableOpacity>
        </View>
    );
};

export default ConfirmOrder;
