import React, {useContext, useEffect, useState} from 'react';
import {View, Text, TouchableOpacity, Image} from "react-native";
import {useNavigation, useRoute} from "@react-navigation/native";
import {UserContext} from "../context/BaseContext";
import VerificationTimer from "../components/VerificationTimer";
import MaskInput from "react-native-mask-input/src/MaskInput";
import axios from "axios";
import {SIGN_IN} from "../utils/urls";
import AsyncStorage from "@react-native-async-storage/async-storage";


const VerificationCode = () => {
    const route = useRoute()
    const resetCode = route.params?.onLogin
    const [code, setCode] = useState('')
    const [seconds, setSeconds] = useState(60);
    const navigation = useNavigation()

    const user = useContext(UserContext)

    const verificationCode = async () => {
        try {

            const response = await axios.post(SIGN_IN, {
                user_id: user.userID,
                verification_code: code
            });
            await AsyncStorage.setItem('token', response.data.token)
            user.token = response.data.token
            navigation.navigate('ChoiceRoles')
        } catch (error) {
            console.log(error.response.data.detail, 'error')
        }
    }

    useEffect(() => {
        if (code.length === 5) {
            verificationCode()
        }
    }, [code])

    return (
        <View className='flex-1 bg-gray-100 relative'>
            <TouchableOpacity className='absolute top-5 left-5' onPress={() => navigation.navigate('Login')}>
                <Text className='text-[16px] text-gray-400'>Back</Text>
            </TouchableOpacity>

            <View className='flex justify-center h-screen w-full p-5'>
                <View className="w-[70px] h-[155px] mb-6">
                    <Image className='w-full h-full' source={require('../../assets/Point-down.png')}/>
                </View>

                <Text className='text-[24px] font-bold'>Confirm your phone number</Text>

                <Text className='text-[24px] font-bold mb-6'>+{user.phone}</Text>

                <Text className='text-[14px] text-gray-500 mb-2'>Enter the SMS code</Text>

                <View className='w-[50%] flex-row items-center mb-32'>
                    <MaskInput
                        className='w-full text-[22px] py-4 text-gray-700'
                        placeholder='• • • • •'
                        autoCorrect={false}
                        keyboardType='numeric'
                        value={code}
                        textContentType='telephoneNumber'
                        onChangeText={text => setCode(text)}
                        mask={[/\d/, /\d/, /\d/, /\d/, /\d/,]}
                    />

                    {seconds ?
                        <VerificationTimer seconds={seconds} setSeconds={setSeconds}/>
                        :
                        <TouchableOpacity onPress={() => {
                            setSeconds(60)
                            resetCode()
                        }}>
                            <Text className='text-[16px]'>Resend</Text>
                        </TouchableOpacity>
                    }
                </View>
            </View>
        </View>
    )
}

export default VerificationCode;
