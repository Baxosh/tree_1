import React from 'react';
import {Image, Text, TouchableOpacity, View} from "react-native";
import {useNavigation} from "@react-navigation/native";

const Success = () => {
    const navigation = useNavigation()

    return (
        <View className='flex-1 bg-gray-200 justify-center items-center'>
            <Text className="text-[28px] font-semibold w-[70%] text-center mb-10">Done successfully</Text>
            <View className="bg-white rounded-full p-8">
                <Image className="w-48 h-48" source={require('../../assets/1.png')}/>
            </View>
            <TouchableOpacity className="w-[90%] px-4 py-3 bg-gray-600 rounded-xl mt-10" onPress={()=> navigation.navigate('ClientHome')}>
                <Text className="text-[22px] text-white text-center">Go to home page</Text>
            </TouchableOpacity>
        </View>
    );
};

export default Success;
