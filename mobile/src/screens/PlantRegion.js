import React from 'react';
import {Text, View, ScrollView, TouchableOpacity} from "react-native";
import {useNavigation} from "@react-navigation/native";
import BackButton from "../components/BackButton";

const regions = [
    {label: "Tashkent city", value: "TOSHKENT_SHAXAR"},
    {label: "Tashkent region", value: "TOSHKENT_VILOYAT"},
    {label: "Bukhara", value: "BUXORO"},
    {label: "Andijan", value: "ANDIJON"},
    {label: "Ferghana", value: "FARGONA"},
    {label: "Namangan", value: "NAMANGAN"},
    {label: "Jizzakh", value: "JIZZAX"},
    {label: "SyrDarya", value: "SIRDARYO"},
    {label: "Samarkand", value: "SAMARQAND"},
    {label: "Navoi", value: "NANOIY"},
    {label: "Kashkadarya", value: "QASHQADARYO"},
    {label: "Surkhandarya", value: "SURXONDARYO"},
    {label: "Khorezm", value: "XORAZM"},
    {label: "Karakalpakstan", value: "QORAQALPOGISTON"},
];

const PlantRegion = () => {
    const navigation = useNavigation()

    return (
        <ScrollView className="flex-1 bg-gray-200">
            <View className="items-center p-5">
                <BackButton/>
                <Text className="text-[26px] font-semibold text-center mb-8 text-gray-500">Select a region</Text>
                {regions.map(region => (
                    <TouchableOpacity onPress={() => navigation.navigate('PlantLocation', {region: region.value})}
                                      className="w-full px-4 py-6 bg-white rounded-xl mb-4" key={region.value}>
                        <Text className="text-[22px] font-semibold text-gray-500">{region.label}</Text>
                    </TouchableOpacity>
                ))}
            </View>
        </ScrollView>
    );
};

export default PlantRegion;
