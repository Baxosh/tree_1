import React from 'react';
import {Text, View, ScrollView, TouchableOpacity, Linking} from "react-native";
import {useNavigation} from "@react-navigation/native";
import BackButton from "../components/BackButton";

const Transaction = () => {
    const navigation = useNavigation()

    return (
        <View className="flex-1 min-h-screen bg-gray-200 p-5 items-center">
            <BackButton/>
            <Text className="text-[26px] font-semibold text-center mb-8 text-gray-500">Payment</Text>
        </View>
    );
};

export default Transaction;
