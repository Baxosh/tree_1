import React, {useContext} from 'react';
import {SafeAreaView, Text, TouchableOpacity, View} from "react-native";
import {UserContext} from "../context/BaseContext";
import {useNavigation} from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";

const ChoiceRoles = () => {
    const user = useContext(UserContext)
    const navigation = useNavigation()
    const saveRoles = (role) => {
        user.role = role
        AsyncStorage.setItem('role', role)
        navigation.navigate('ChooseRegion')
    }
    return (
        <SafeAreaView className='flex-1 bg-gray-200 justify-center items-center'>
            <Text className="text-[27px] text-gray-600 text-center font-bold">
                Choose one of the roles
            </Text>
            <View className="flex-row justify-center items-center">
                <TouchableOpacity
                    onPress={() => saveRoles('donator')}
                    className="justify-between items-center w-[43%] h-[49%] bg-white rounded-lg mr-8">
                    <View className='w-full h-full flex justify-center items-center'>
                        <Text className="text-[17px] text-gray-400 text-center font-bold">
                            I want to donate
                        </Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => saveRoles('client')}
                    className="justify-between w-[43%] h-[49%] bg-white rounded-lg">
                    <View className='w-full h-full flex justify-center items-center'>
                        <Text className="text-[17px] text-gray-400 text-center font-bold">
                            I want to plant a tree
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    );
};

export default ChoiceRoles;
