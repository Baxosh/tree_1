import React, {useState} from 'react';
import {Text, View, ScrollView, TouchableOpacity, Dimensions, TextInput} from "react-native";
import {useNavigation, useRoute} from "@react-navigation/native";
import BackButton from "../components/BackButton";
import MapView, {PROVIDER_GOOGLE} from "react-native-maps";
import DropDownPicker from "react-native-dropdown-picker";
import MaskInput from "react-native-mask-input/src/MaskInput";

const ClientOutMoney = () => {
    const [summa, setSumma] = useState('')
    const [card, setCard] = useState('')

    const route = useRoute()
    const navigation = useNavigation()

    return (
        <View className="flex-1 min-h-screen bg-gray-200 p-5 items-center">
            <BackButton/>

            <Text className="text-[26px] font-semibold text-center mb-8 text-gray-500">Withdraw funds</Text>

            <View className="flex-1 mb-10 w-full">
                <View className='w-full rounded-xl mb-8 bg-black/70 p-5'>
                    <Text className="text-[24px] text-white">Balance</Text>
                    <Text className="text-[28px] font-semibold text-right text-white">$260</Text>
                </View>

                <Text className="text-gray-700 mx-2 mb-1 text-[18px] font-semibold">Summa</Text>
                <TextInput
                    className='w-full border-0 px-3 py-4 rounded-xl bg-white text-gray-700 text-[18px] mb-8'
                    onChangeText={setSumma}
                    value={summa}
                    keyboardType={"numeric"}
                    placeholder="$ ***"
                />

                <Text className="text-gray-700 mx-2 mb-1 text-[18px] font-semibold">Card number</Text>
                <MaskInput
                    className='w-full border-0 px-3 py-4 rounded-xl bg-white text-gray-700 text-[18px] mb-8'
                    value={card}
                    autoCorrect={false}
                    keyboardType='numeric'
                    onChangeText={(masked, unmasked) => {
                        setCard(unmasked);
                    }}
                    placeholder="**** **** **** ****"
                    mask={[/\d/,/\d/, /\d/, /\d/, ' ',/\d/,/\d/, /\d/, /\d/, ' ',/\d/,/\d/, /\d/, /\d/, ' ',/\d/,/\d/, /\d/, /\d/, ]}
                />
            </View>

            <TouchableOpacity
                onPress={() => navigation.navigate('Transaction')}
                disabled={Boolean(card && summa)}
                className={`absolute bottom-5 w-full px-4 py-3 rounded-xl mb-4 ${card && summa? 'bg-black/50' : 'bg-black/70'}`}>
                <Text className="text-[22px] font-semibold text-white text-center">Send</Text>
            </TouchableOpacity>
        </View>
    );
};

export default ClientOutMoney;
