import React, {useState} from 'react';
import {Text, View, ScrollView} from "react-native";
import {useNavigation} from "@react-navigation/native";
import BackButton from "../components/BackButton";
import {PieChart} from "react-native-gifted-charts";

const Statistics = () => {

    const navigation = useNavigation()

    const data = [
        {value: 800, color: '#25ad21'},
        {value: 600, color: '#1b5765'},
    ]

    return (
        <ScrollView className="flex-1 bg-gray-200">
            <BackButton/>
            <View className="items-center p-2">
                <Text className="text-[26px] mt-3 mb-2 text-gray-500">Rating of All Investor</Text>
                <View className="w-[90%] mx-auto mt-10 justify-center items-center bg-white rounded-xl p-3">
                    <Text className="text-[22px] mb-2">Total 1400 orders</Text>
                    <PieChart
                        data={data}
                        textSize={20}
                    />
                    <View className="flex-row items-center mt-3 w-full">
                        <View className='w-5 h-5 rounded-full bg-[#25ad21] mr-2'></View>
                        <Text className='text-[18px] mr-2'>Completed (Planted):</Text>
                        <Text className='text-[22px] font-[500]'>{data[0].value}</Text>
                    </View>
                    <View className="flex-row items-center mt-3 w-full">
                        <View className='w-5 h-5 rounded-full bg-[#1b5765] mr-2'></View>
                        <Text className='text-[18px] mr-2'>In Process:</Text>
                        <Text className='text-[22px] font-[500]'>{data[1].value}</Text>
                    </View>
                </View>
            </View>
        </ScrollView>
    );
};

export default Statistics;
