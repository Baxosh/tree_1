import React, {useContext, useEffect, useState} from 'react';
import {View, Text, TouchableOpacity, Image} from "react-native";
import {useNavigation} from "@react-navigation/native";
import MaskInput from "react-native-mask-input/src/MaskInput";
import {UserContext} from "../context/BaseContext";
import {SEND_CODE} from "../utils/urls";
import axios from 'axios'
import AsyncStorage from "@react-native-async-storage/async-storage";

const Login = () => {
    const [phone, setPhone] = useState('998')
    const [disabled, setDisabled] = useState(true)
    const navigation = useNavigation()
    const user = useContext(UserContext)
    const [userToken, setUserToken] = useState('')
    const [roles, setRoles] = useState('')
    AsyncStorage.getItem('token').then(r => setUserToken(r))
    AsyncStorage.getItem('role').then(r => setRoles(r))
    const onLogin = async () => {
        try {
            const response = await axios.post(SEND_CODE, {
                phone: phone
            });
            user.userID = response.data.user_id
            user.phone = phone
        } catch (error) {
            console.log(error.response, 'error')
        }
    }


    useEffect(() => {
        if (userToken) {
            if (roles === 'donator') {
                navigation.navigate('Home')
            } else {
                navigation.navigate('ClientHome')
            }
        }
    }, [userToken])

    return (
        <View className='flex-1 bg-gray-100'>
            <View className='flex justify-center h-screen w-full p-5'>
                <View className="w-[190px] h-[130px] my-10">
                    <Image className='w-full h-full' source={require('../../assets/Phone-min.png')}/>
                </View>
                <View>
                    <Text className='text-[24px] font-bold mb-6'>Authorization</Text>
                    <Text className='text-[14px] text-gray-500 mb-2'>Enter your phone number</Text>
                    <MaskInput
                        className='w-full text-[18px] border-2 border-gray-300 px-3 py-4 rounded-xl bg-gray-200 text-gray-700'
                        value={phone}
                        autoCorrect={false}
                        keyboardType='numeric'
                        onChangeText={(masked, unmasked) => {
                            setPhone(unmasked);
                            if (unmasked.length === 12) {
                                setDisabled(false)
                            } else {
                                setDisabled(true)
                            }
                        }}
                        placeholder="Phone number"
                        mask={['+', /\d/, /\d/, /\d/, ' ', '(', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/,]}
                    />
                    <TouchableOpacity
                        disabled={disabled}
                        onPress={() => {
                            onLogin()
                            navigation.navigate('Verification', {onLogin})
                        }}
                        className={`w-full rounded-xl mt-4 ${disabled ? 'bg-black/50' : 'bg-black/70'}`}
                    >
                        <Text className="text-[18px] font-semibold text-center text-white px-3 py-5 uppercase">
                            Continue
                        </Text>
                    </TouchableOpacity>
                </View>

                <Text className="text-[12px] text-gray-500 mb-2 text-center underline leading-4 mt-20">
                    By entering your phone number, you agree to the terms of the user agreement and processing policy
                    you accept
                </Text>
            </View>
        </View>
    )
}

export default Login;
