import React, {useState} from 'react';
import {Text, View, ScrollView, TouchableOpacity} from "react-native";
import {useFocusEffect, useNavigation, useRoute} from "@react-navigation/native";
import BackButton from "../components/BackButton";
import axios from "axios";
import {PLANT} from "../utils/urls";
import AsyncStorage from "@react-native-async-storage/async-storage";

const ClientOrderLocations = () => {
    const route = useRoute()
    const region = route.params?.region
    const [locations, setLocations] = useState([])
    const navigation = useNavigation()

    const getLocationPlant = async () => {
        try {
            const token = await AsyncStorage.getItem('token')
            const response = await axios.get(PLANT, {
                params: {
                    region
                }, headers: {
                    Authorization: `Token ${token}`
                }
            })
            setLocations(response.data)
        } catch (error) {
            console.log(error.response.data.detail)
        }
    }

    useFocusEffect(React.useCallback(() => {
        getLocationPlant()
    }, []),)


    const filterRegions = (id) => {
        let res = {}
        for (let item of locations) {
            if (id === item.id) {
                res = {...res, item}
            }
        }
        navigation.navigate('ClientOrderInfo', {locationResult: res})
    }

    return (
        <View className="flex-1 min-h-screen bg-gray-200 p-5 items-center">
            <BackButton/>
            <Text className="text-[26px] font-semibold text-center mb-8 text-gray-500">Locations</Text>
            <ScrollView className="flex-1 mb-20 w-full"  showsVerticalScrollIndicator={false}>
                {locations.map((location) => (
                    <TouchableOpacity onPress={() => filterRegions(location.id)}
                                      className="w-full px-4 py-4 bg-white rounded-xl mb-4" key={location.id}>
                        <Text className="text-[22px] font-semibold text-gray-500">{location?.address}</Text>
                    </TouchableOpacity>
                ))}
            </ScrollView>

            <TouchableOpacity onPress={() => navigation.navigate('ClientMap', {locations})}
                              className="absolute bottom-5 w-full px-4 py-3 bg-gray-600 rounded-xl mb-4">
                <Text className="text-[22px] font-semibold text-white text-center">Map</Text>
            </TouchableOpacity>
        </View>
    );
};

export default ClientOrderLocations;
