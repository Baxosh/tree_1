import React, {useState} from 'react';
import {Text, View, ScrollView, TouchableOpacity, Dimensions, TextInput} from "react-native";
import {useNavigation, useRoute} from "@react-navigation/native";
import BackButton from "../components/BackButton";
import MapView, {PROVIDER_GOOGLE} from "react-native-maps";
import DropDownPicker from "react-native-dropdown-picker";

const treeTypes = [
    {label: 'Archa', value: "archa"},
    {label: 'Archacha', value: "archacha"},
    {label: 'chinor', value: "chinor"},
]

const ClientOrderInfo = () => {
    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(null);
    const [items, setItems] = useState(treeTypes);
    const [count, setCount] = useState()

    const route = useRoute()
    const resultLocation = route.params?.locationResult?.item

    const navigation = useNavigation()
    const {width, height} = Dimensions.get("window")

    const ASPECT_RATIO = width / height;
    const LATITUDE_DELTA = 0.1;
    const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
    const INITIAL_POSITION = {
        latitude: 39.77472,
        longitude: 64.42861,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
    };

    return (
        <View className="flex-1 min-h-screen bg-gray-200 p-5 items-center">
            <BackButton/>
            <Text className="text-[26px] font-semibold text-center mb-8 text-gray-500">Order information</Text>
            <View className="flex-1 mb-10 w-full">
                <View className='w-full h-32 rounded-xl overflow-hidden mb-4'>
                    <MapView
                        className='w-full h-full'
                        provider={PROVIDER_GOOGLE}
                        initialRegion={INITIAL_POSITION}
                        mapType={'standard'} // satellite
                    >
                    </MapView>
                </View>

                <View className="w-full px-4 py-6 bg-white rounded-xl mb-4">
                    <Text className="text-[22px] font-semibold text-gray-500">Number of trees: 10</Text>
                </View>

                <View className="w-full px-4 py-6 bg-white rounded-xl mb-4">
                    <Text className="text-[22px] font-semibold text-gray-500">$40</Text>
                </View>

            </View>

            <TouchableOpacity
                onPress={() => navigation.navigate('Success')}
                className="absolute bottom-5 w-full px-4 py-3 bg-[#25ba3e] rounded-xl mb-4">
                <Text className="text-[22px] font-semibold text-white text-center">Acceptance</Text>
            </TouchableOpacity>
        </View>
    );
};

export default ClientOrderInfo;
