import React, {useState} from 'react';
import {Text, View, ScrollView, TouchableOpacity} from "react-native";
import {useFocusEffect, useNavigation, useRoute} from "@react-navigation/native";
import BackButton from "../components/BackButton";
import axios from "axios";
import {PLANT} from "../utils/urls";
import AsyncStorage from "@react-native-async-storage/async-storage";

const ClientMyOrders = () => {
    const route = useRoute()
    const navigation = useNavigation()

    // const getLocationPlant = async () => {
    //     try {
    //         const token = await AsyncStorage.getItem('token')
    //         const response = await axios.get(PLANT, {
    //             params: {
    //                 region
    //             }, headers: {
    //                 Authorization: `Token ${token}`
    //             }
    //         })
    //         setLocations(response.data)
    //     } catch (error) {
    //         console.log(error.response.data.detail)
    //     }
    // }
    //
    // useFocusEffect(React.useCallback(() => {
    //     getLocationPlant()
    // }, []),)


    // const filterRegions = (id) => {
    //     let res = {}
    //     for (let item of locations) {
    //         if (id === item.id) {
    //             res = {...res, item}
    //         }
    //     }
    //     navigation.navigate('ConfirmOrder', {locationResult: res})
    // }

    return (
        <View className="flex-1 min-h-screen bg-gray-200 p-5 items-center">
            <BackButton/>

            <Text className="text-[26px] font-semibold text-center mb-8 text-gray-500">Order history</Text>

            <ScrollView className="flex-1 mb-16 w-full" showsVerticalScrollIndicator={false}>
                <TouchableOpacity onPress={()=>navigation.navigate('ClientCompleteOrder')} className="w-full px-4 py-4 bg-white rounded-xl mb-4 flex-row justify-between items-center">
                    <Text className="text-[22px] font-semibold text-gray-500">Bukhara</Text>
                    <View className="w-10 h-10 bg-[#2bc246] rounded-full" />
                </TouchableOpacity>
            </ScrollView>

            <TouchableOpacity onPress={() => navigation.navigate('PlantLocationMap', {locations})}
                              className="absolute bottom-5 w-full px-4 py-3 bg-white rounded-xl mb-4">
                <Text className="text-[22px] font-semibold text-gray-500 text-center">Map</Text>
            </TouchableOpacity>
        </View>
    );
};

export default ClientMyOrders;
