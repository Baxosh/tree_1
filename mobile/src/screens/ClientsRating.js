import React, {useState} from 'react';
import {Text, View, Image, ScrollView, TouchableOpacity, Button, Pressable} from "react-native";
import {useFocusEffect, useNavigation} from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import BackButton from "../components/BackButton";
import MaskInput from "react-native-mask-input/src/MaskInput";
import {DONATOR_RATING, PLANT} from "../utils/urls";
import axios from "axios";

const donators = [
    {id: 1, name: 'Johnson', plantedTrees: 1000},
    {id: 2, name: 'Bekjan', plantedTrees: 980},
    {id: 3, name: 'Hoshimbek', plantedTrees: 820},
    {id: 4, name: 'Botir', plantedTrees: 800},
    {id: 5, name: 'Shoxi', plantedTrees: 789},
    {id: 6, name: 'Jonibek', plantedTrees: 765},
    {id: 7, name: 'Shoyimjon', plantedTrees: 690},
    {id: 8, name: 'Alex', plantedTrees: 612},
    {id: 9, name: 'John', plantedTrees: 567},
    {id: 10, name: 'Alibek', plantedTrees: 400},
]

const ClientsRating = () => {

    return (
        <ScrollView className="flex-1 bg-gray-200">
            <BackButton/>
            <View className="items-center p-2">
                <Text className="text-[26px] mt-4 text-gray-500">Rating of All Users</Text>

                <View className="w-full gap-3 p-5">
                    <Text className="text-[28px] mb-2 text-center font-semibold">Top 10 Users</Text>

                    {donators ? donators.map((donator, index) => (
                        <View
                            key={donator.id}
                            className="flex-row rounded-xl items-center p-3 bg-white"
                        >
                            <Text className="text-[22px] text-center mr-2">
                                {index + 1}
                            </Text>
                            <Text className="text-[22px] text-center">
                                {donator.name}
                            </Text>
                            <View className="ml-auto">
                                <Text className="text-[16px] text-center text-green-500">
                                    Planted trees:
                                </Text>
                                <Text className="text-[20px] text-center font-semibold text-gray-900">
                                    {donator.plantedTrees}
                                </Text>
                            </View>
                        </View>
                    )): null}
                </View>
            </View>
        </ScrollView>
    );
};

export default ClientsRating;
