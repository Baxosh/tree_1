import React, {useState} from 'react';
import {Text, View, Image, ScrollView, TouchableOpacity} from "react-native";
import {useFocusEffect, useNavigation} from "@react-navigation/native";
import {Svg, G, Path} from "react-native-svg";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import {USER} from "../utils/urls";

const ClientHome = () => {
    const navigation = useNavigation()
    const [userData, setUserData] = useState([])

    const getUserData = async () => {
        const token = await AsyncStorage.getItem('token')
        const userId = await AsyncStorage.getItem('userId')
        try {
            const response = await axios.get(`${USER}${userId}`, {
                headers: {
                    Authorization: `Token ${token}`
                }
            })
            setUserData(response.data)

            await AsyncStorage.setItem('user', JSON.stringify(response.data));
        } catch (error) {
            console.log(error.response)
        }
    }

    useFocusEffect(
        React.useCallback(() => {
            getUserData()
        }, []))

    return (
        <ScrollView className="flex-1 bg-gray-200">
            <View className="items-center p-5">
                <View className="w-full p-2 rounded-xl">
                    <View className="flex-row justify-between">
                        <Image className="w-16 h-16 mb-2" source={require('../../assets/user-default.jpeg')}/>
                        <TouchableOpacity>
                            <Text
                                className="text-[18px] bg-black/50 rounded-full py-1 px-3 text-white">Log out</Text>
                        </TouchableOpacity>
                    </View>

                    <Text className="text-gray-500 text-[18px] font-semibold">+{userData?.phone}</Text>
                </View>

                <TouchableOpacity onPress={() => navigation.navigate('ClientOrderLocations')}
                                  className="w-full h-44 justify-center py-10 bg-[#85C590] rounded-xl my-4 flex-row">
                    <View className="w-[50%]">
                        <Text className="text-[28px] font-semibold text-white">Recent</Text>
                        <Text className="text-[28px] font-semibold text-white">orders</Text>
                    </View>
                    <Image className="w-[108px] h-[160px] scale-125 -mt-[70px] mr-5"
                           source={require('../../assets/tree-3d.png')}/>
                </TouchableOpacity>

                <View className="w-full flex-row justify-between">
                    <TouchableOpacity onPress={() => navigation.navigate('ClientMyOrders')}
                                      className="w-[48%] h-40 p-4 justify-between bg-white rounded-xl">
                        <View className="w-10 h-10 bg-gray-200 items-center justify-center rounded-full">
                            <Svg width="28" height="28" viewBox="0 0 24 24" fill="#6B7280"
                                 xmlns="http://www.w3.org/2000/svg">
                                <G id="Interface / Drag_Horizontal">
                                    <G id="Vector">
                                        <Path
                                            d="M18 14C17.4477 14 17 14.4477 17 15C17 15.5523 17.4477 16 18 16C18.5523 16 19 15.5523 19 15C19 14.4477 18.5523 14 18 14Z"
                                            stroke="#6B7280" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round"/>
                                        <Path
                                            d="M12 14C11.4477 14 11 14.4477 11 15C11 15.5523 11.4477 16 12 16C12.5523 16 13 15.5523 13 15C13 14.4477 12.5523 14 12 14Z"
                                            stroke="#6B7280" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round"/>
                                        <Path
                                            d="M6 14C5.44772 14 5 14.4477 5 15C5 15.5523 5.44772 16 6 16C6.55228 16 7 15.5523 7 15C7 14.4477 6.55228 14 6 14Z"
                                            stroke="#6B7280" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round"/>
                                        <Path
                                            d="M18 8C17.4477 8 17 8.44772 17 9C17 9.55228 17.4477 10 18 10C18.5523 10 19 9.55228 19 9C19 8.44772 18.5523 8 18 8Z"
                                            stroke="#6B7280" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round"/>
                                        <Path
                                            d="M12 8C11.4477 8 11 8.44772 11 9C11 9.55228 11.4477 10 12 10C12.5523 10 13 9.55228 13 9C13 8.44772 12.5523 8 12 8Z"
                                            stroke="#6B7280" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round"/>
                                        <Path
                                            d="M6 8C5.44772 8 5 8.44772 5 9C5 9.55228 5.44772 10 6 10C6.55228 10 7 9.55228 7 9C7 8.44772 6.55228 8 6 8Z"
                                            stroke="#6B7280" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round"/>
                                    </G>
                                </G>
                            </Svg>
                        </View>

                        <Text className="text-[18px] font-semibold text-gray-500">My orders</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => navigation.navigate('ClientOutMoney')}
                                      className="w-[48%] h-40 p-4 justify-between bg-white rounded-xl">
                        <View className="w-10 h-10 bg-gray-200 items-center justify-center rounded-full">
                            <Svg width="28" height="28" viewBox="0 0 24 24" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <G id="Interface / Chart_Line">
                                    <Path id="Vector"
                                          d="M3 15.0002V16.8C3 17.9201 3 18.4798 3.21799 18.9076C3.40973 19.2839 3.71547 19.5905 4.0918 19.7822C4.5192 20 5.07899 20 6.19691 20H21.0002M3 15.0002V5M3 15.0002L6.8534 11.7891L6.85658 11.7865C7.55366 11.2056 7.90288 10.9146 8.28154 10.7964C8.72887 10.6567 9.21071 10.6788 9.64355 10.8584C10.0105 11.0106 10.3323 11.3324 10.9758 11.9759L10.9822 11.9823C11.6357 12.6358 11.9633 12.9635 12.3362 13.1153C12.7774 13.2951 13.2685 13.3106 13.7207 13.1606C14.1041 13.0334 14.4542 12.7275 15.1543 12.115L21 7"
                                          stroke="#6B7280" stroke-width="2" stroke-linecap="round"
                                          stroke-linejoin="round"/>
                                </G>
                            </Svg>

                        </View>
                        <Text className="text-[18px] font-semibold text-gray-500">Withdraw funds to the card</Text>
                    </TouchableOpacity>
                </View>
            </View>

            <View className="w-full flex-row justify-between px-5">
                <TouchableOpacity onPress={() => navigation.navigate('ClientsRating')}
                                  className="w-[48%] h-40 p-4 justify-between bg-white rounded-xl">
                    <View className="w-10 h-10 bg-gray-200 items-center justify-center rounded-full">
                        <Svg width="28" height="28" viewBox="0 0 24 24" fill="#6B7280"
                             xmlns="http://www.w3.org/2000/svg">
                            <G id="Interface / Star">
                                <Path id="Vector"
                                      d="M2.33496 10.3368C2.02171 10.0471 2.19187 9.52339 2.61557 9.47316L8.61914 8.76107C8.79182 8.74059 8.94181 8.63215 9.01465 8.47425L11.5469 2.98446C11.7256 2.59703 12.2764 2.59695 12.4551 2.98439L14.9873 8.47413C15.0601 8.63204 15.2092 8.74077 15.3818 8.76124L21.3857 9.47316C21.8094 9.52339 21.9791 10.0472 21.6659 10.3369L17.2278 14.4419C17.1001 14.56 17.0433 14.7357 17.0771 14.9063L18.255 20.8359C18.3382 21.2544 17.8928 21.5787 17.5205 21.3703L12.2451 18.4166C12.0934 18.3317 11.9091 18.3321 11.7573 18.417L6.48144 21.3695C6.10913 21.5779 5.66294 21.2544 5.74609 20.8359L6.92414 14.9066C6.95803 14.7361 6.90134 14.5599 6.77367 14.4419L2.33496 10.3368Z"
                                      stroke="none" stroke-width="2" stroke-linecap="round"
                                      stroke-linejoin="round"/>
                            </G>
                        </Svg>
                    </View>
                    <Text className="text-[18px] font-semibold text-gray-500">Rating</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => navigation.navigate('Settings')}
                                  className="w-[48%] h-40 p-4 justify-between bg-white rounded-xl">
                    <View className="w-10 h-10 bg-gray-200 items-center justify-center rounded-full">
                        <Svg width="28" height="28" viewBox="0 0 24 24" fill="#6B7280"
                             xmlns="http://www.w3.org/2000/svg">
                            <G id="Interface / Settings">
                                <G id="Vector">
                                    <Path
                                        d="M20.3499 8.92293L19.9837 8.7192C19.9269 8.68756 19.8989 8.67169 19.8714 8.65524C19.5983 8.49165 19.3682 8.26564 19.2002 7.99523C19.1833 7.96802 19.1674 7.93949 19.1348 7.8831C19.1023 7.82677 19.0858 7.79823 19.0706 7.76998C18.92 7.48866 18.8385 7.17515 18.8336 6.85606C18.8331 6.82398 18.8332 6.79121 18.8343 6.72604L18.8415 6.30078C18.8529 5.62025 18.8587 5.27894 18.763 4.97262C18.6781 4.70053 18.536 4.44993 18.3462 4.23725C18.1317 3.99685 17.8347 3.82534 17.2402 3.48276L16.7464 3.1982C16.1536 2.85658 15.8571 2.68571 15.5423 2.62057C15.2639 2.56294 14.9765 2.56561 14.6991 2.62789C14.3859 2.69819 14.0931 2.87351 13.5079 3.22396L13.5045 3.22555L13.1507 3.43741C13.0948 3.47091 13.0665 3.48779 13.0384 3.50338C12.7601 3.6581 12.4495 3.74365 12.1312 3.75387C12.0992 3.7549 12.0665 3.7549 12.0013 3.7549C11.9365 3.7549 11.9024 3.7549 11.8704 3.75387C11.5515 3.74361 11.2402 3.65759 10.9615 3.50224C10.9334 3.48658 10.9056 3.46956 10.8496 3.4359L10.4935 3.22213C9.90422 2.86836 9.60915 2.69121 9.29427 2.62057C9.0157 2.55807 8.72737 2.55634 8.44791 2.61471C8.13236 2.68062 7.83577 2.85276 7.24258 3.19703L7.23994 3.1982L6.75228 3.48124L6.74688 3.48454C6.15904 3.82572 5.86441 3.99672 5.6517 4.23614C5.46294 4.4486 5.32185 4.69881 5.2374 4.97018C5.14194 5.27691 5.14703 5.61896 5.15853 6.3027L5.16568 6.72736C5.16676 6.79166 5.16864 6.82362 5.16817 6.85525C5.16343 7.17499 5.08086 7.48914 4.92974 7.77096C4.9148 7.79883 4.8987 7.8267 4.86654 7.88237C4.83436 7.93809 4.81877 7.96579 4.80209 7.99268C4.63336 8.26452 4.40214 8.49186 4.12733 8.65572C4.10015 8.67193 4.0715 8.68752 4.01521 8.71871L3.65365 8.91908C3.05208 9.25245 2.75137 9.41928 2.53256 9.65669C2.33898 9.86672 2.19275 10.1158 2.10349 10.3872C2.00259 10.6939 2.00267 11.0378 2.00424 11.7255L2.00551 12.2877C2.00706 12.9708 2.00919 13.3122 2.11032 13.6168C2.19979 13.8863 2.34495 14.134 2.53744 14.3427C2.75502 14.5787 3.05274 14.7445 3.64974 15.0766L4.00808 15.276C4.06907 15.3099 4.09976 15.3266 4.12917 15.3444C4.40148 15.5083 4.63089 15.735 4.79818 16.0053C4.81625 16.0345 4.8336 16.0648 4.8683 16.1255C4.90256 16.1853 4.92009 16.2152 4.93594 16.2452C5.08261 16.5229 5.16114 16.8315 5.16649 17.1455C5.16707 17.1794 5.16658 17.2137 5.16541 17.2827L5.15853 17.6902C5.14695 18.3763 5.1419 18.7197 5.23792 19.0273C5.32287 19.2994 5.46484 19.55 5.65463 19.7627C5.86915 20.0031 6.16655 20.1745 6.76107 20.5171L7.25478 20.8015C7.84763 21.1432 8.14395 21.3138 8.45869 21.379C8.73714 21.4366 9.02464 21.4344 9.30209 21.3721C9.61567 21.3017 9.90948 21.1258 10.4964 20.7743L10.8502 20.5625C10.9062 20.5289 10.9346 20.5121 10.9626 20.4965C11.2409 20.3418 11.5512 20.2558 11.8695 20.2456C11.9015 20.2446 11.9342 20.2446 11.9994 20.2446C12.0648 20.2446 12.0974 20.2446 12.1295 20.2456C12.4484 20.2559 12.7607 20.3422 13.0394 20.4975C13.0639 20.5112 13.0885 20.526 13.1316 20.5519L13.5078 20.7777C14.0971 21.1315 14.3916 21.3081 14.7065 21.3788C14.985 21.4413 15.2736 21.4438 15.5531 21.3855C15.8685 21.3196 16.1657 21.1471 16.7586 20.803L17.2536 20.5157C17.8418 20.1743 18.1367 20.0031 18.3495 19.7636C18.5383 19.5512 18.6796 19.3011 18.764 19.0297C18.8588 18.7252 18.8531 18.3858 18.8417 17.7119L18.8343 17.2724C18.8332 17.2081 18.8331 17.1761 18.8336 17.1445C18.8383 16.8247 18.9195 16.5104 19.0706 16.2286C19.0856 16.2007 19.1018 16.1726 19.1338 16.1171C19.166 16.0615 19.1827 16.0337 19.1994 16.0068C19.3681 15.7349 19.5995 15.5074 19.8744 15.3435C19.9012 15.3275 19.9289 15.3122 19.9838 15.2818L19.9857 15.2809L20.3472 15.0805C20.9488 14.7472 21.2501 14.5801 21.4689 14.3427C21.6625 14.1327 21.8085 13.8839 21.8978 13.6126C21.9981 13.3077 21.9973 12.9658 21.9958 12.2861L21.9945 11.7119C21.9929 11.0287 21.9921 10.6874 21.891 10.3828C21.8015 10.1133 21.6555 9.86561 21.463 9.65685C21.2457 9.42111 20.9475 9.25526 20.3517 8.92378L20.3499 8.92293Z"
                                        stroke="#E5E7EB" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"/>
                                    <Path
                                        d="M8.00033 12C8.00033 14.2091 9.79119 16 12.0003 16C14.2095 16 16.0003 14.2091 16.0003 12C16.0003 9.79082 14.2095 7.99996 12.0003 7.99996C9.79119 7.99996 8.00033 9.79082 8.00033 12Z"
                                        stroke="#E5E7EB" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"/>
                                </G>
                            </G>
                        </Svg>
                    </View>

                    <Text className="text-[18px] font-semibold text-gray-500">Settings</Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    )
        ;
};

export default ClientHome;
