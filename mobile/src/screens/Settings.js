import React, {useState} from 'react';
import {Text, View, Image, ScrollView, TouchableOpacity, Button, Pressable} from "react-native";
import {useFocusEffect, useNavigation} from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import BackButton from "../components/BackButton";
import MaskInput from "react-native-mask-input/src/MaskInput";

const Settings = () => {
    const [phone, setPhone] = useState('')
    const [name, setName] = useState('')
    const [disabled, setDisabled] = useState(true)

    const navigation = useNavigation()

    useFocusEffect(
        React.useCallback(() => {
            getUserData()
        }, []))

    const getUserData = async () => {
        try {
            const userData = await AsyncStorage.getItem('user')
            const data = JSON.parse(userData)
            setPhone(data.phone)
            setName(data.name)
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <ScrollView className="flex-1 bg-gray-200">
            <BackButton/>
            <View className="items-center p-5">
                <Text className="text-[26px] mb-10 text-gray-500">Settings</Text>
                <View className="py-5 items-center">
                    <Image className="w-16 h-16 mb-2" source={require('../../assets/user-default.jpeg')}/>
                    <Text className="text-[20px] font-semibold">{name}</Text>
                </View>
                <View
                    className="w-full border-2 border-gray-300 bg-gray-200 rounded-xl flex-row items-center justify-between">
                    <MaskInput
                        className='text-[18px] px-3 py-4 text-gray-700'
                        value={phone}
                        autoCorrect={false}
                        editable={!disabled}
                        keyboardType='numeric'
                        autoFocus={!disabled}
                        onChangeText={(masked, unmasked) => {
                            setPhone(unmasked);
                        }}
                        placeholder="Telefon nomer"
                        mask={['+', /\d/, /\d/, /\d/, ' ', '(', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/,]}
                    />
                    {disabled ? (
                        <Pressable className="bg-gray-600 rounded-full px-3 py-[2px] mr-2"
                                   onPress={() => setDisabled(false)}>
                            <Text className="text-[16px] text-white">Edit</Text>
                        </Pressable>
                    ) : null}

                </View>
                <TouchableOpacity className="w-full px-4 py-3 bg-gray-600 rounded-xl mt-10"
                                  onPress={() => navigation.goBack()}>
                    <Text className="text-[22px] text-white text-center">Save</Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
};

export default Settings;
