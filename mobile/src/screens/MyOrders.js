import React, {useState} from 'react';
import {Text, View, ScrollView, TouchableOpacity} from "react-native";
import BackButton from "../components/BackButton";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import {DONATORS_ORDERS} from "../utils/urls";
import {useFocusEffect} from "@react-navigation/native";

const MyOrders = () => {
    const [orders, setOrders] = useState(null)

    const getMyOrders = async () => {
        try {
            const token = await AsyncStorage.getItem('token')
            const user = await AsyncStorage.getItem('user')
            const {id} = await JSON.parse(user)

            const response = await axios.get(DONATORS_ORDERS.replace('id', id), {
                headers: {
                    Authorization: `Token ${token}`
                }
            })
            setOrders(response.data)
        } catch (error) {
            console.log(error, 'error')
        }
    }

    useFocusEffect(
        React.useCallback(() => {
            getMyOrders()
        }, []))

    return (
        <ScrollView className="flex-1 bg-gray-200">
            <BackButton/>
            <View className="items-center p-5">
                <Text className="text-[26px] mb-10 text-gray-500">My Orders</Text>
                {orders && orders.length ? (
                    orders.map(order => (
                        <View className="w-full px-4 py-3 bg-white rounded-xl mt-10" key={order.id}>
                            <View className="flex-row items-end">
                                <Text className="text-[18px] mr-2">Location:</Text>
                                <Text className="text-[22px] font-semibold">{order.donator_region}</Text>
                            </View>
                            <View className="flex-row items-end">
                                <Text className="text-[18px] mr-2">Status:</Text>
                                <Text className="text-[22px] font-semibold">{order.status}</Text>
                            </View>
                            <View className="flex-row items-end">
                                <Text className="text-[18px] mr-2">Count:</Text>
                                <Text className="text-[22px] font-semibold">{order.count}</Text>
                            </View>
                        </View>
                    ))
                ) : (
                    <Text className="text-[22px] w-full text-center font-bold">You didn't donated yet</Text>
                )}
            </View>
        </ScrollView>
    );
};

export default MyOrders;
