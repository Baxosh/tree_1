import React, {useState} from 'react';
import {Image, Text, TouchableOpacity, View} from "react-native";
import BackButton from "../components/BackButton";
import * as ImagePicker from "react-native-image-picker";
import {useNavigation} from "@react-navigation/native";

function ClientCompleteOrder() {
    const [confirmImage, setConfirmImage] = useState('')
    const navigation = useNavigation()

    const options = {
        mediaType: 'photo',
        multiple: false,
    };

    const openImagePicker = () => {
        ImagePicker.launchImageLibrary(options, async (response) => {
            if (response.didCancel) {
                console.log('Выбор изображения отменен');
            } else if (response.error) {
                console.log('Ошибка выбора изображения:', response.error);
            } else {
                for (let i of response['assets']) {
                    setConfirmImage(i.uri);
                    console.log(i.uri)
                    // changeUserData(i.uri, i.type, i.fileName)
                }
            }
        });
    };

    console.log(confirmImage)
    return (
        <View className="flex-1 p-5">
            <BackButton/>
            <Text className="text-[26px] font-semibold text-center mb-8 text-gray-500">Upload Image</Text>
            <TouchableOpacity onPress={openImagePicker}
                              className={`w-full h-[50vh] mt-20 rounded-xl items-center justify-center ${confirmImage ? 'bg-gray-100' : 'bg-gray-100'}`}>
                {/*{confirmImage ? <Image source={{ uri: confirmImage }} /> : null}*/}
                <Image source={{uri: confirmImage}} style={{ width: 200, height: 200 }} />

                <Text
                    className={`w-full border-0 px-3 py-4 rounded-xl text-[18px] mb-6 text-center text-black`}>
                    {confirmImage ? 'Image Uploaded' : 'Image Not Uploaded'}
                </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => confirmImage && navigation?.navigate('Success')}
                              className={`absolute bottom-5 w-full px-4 py-3 rounded-xl mb-4 mx-5 ${confirmImage ? 'bg-black/80' : 'bg-black/50'}`}>
                <Text className="text-[22px] font-semibold text-white text-center">Finish</Text>
            </TouchableOpacity>
        </View>
    );
}

export default ClientCompleteOrder;
