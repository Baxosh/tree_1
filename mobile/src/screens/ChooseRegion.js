import React, {useContext, useState} from 'react';
import {SafeAreaView, Text, TextInput, TouchableOpacity, View, Image} from "react-native";
import {useNavigation} from "@react-navigation/native";
import DropDownPicker from "react-native-dropdown-picker";
import axios from "axios";
import {CHANGE_USER, USER} from "../utils/urls";
import {UserContext} from "../context/BaseContext";
import AsyncStorage from "@react-native-async-storage/async-storage";
import * as ImagePicker from 'react-native-image-picker'


const regions = [
    {label: "Tashkent city", value: "TOSHKENT_SHAXAR"},
    {label: "Tashkent region", value: "TOSHKENT_VILOYAT"},
    {label: "Bukhara", value: "BUXORO"},
    {label: "Andijan", value: "ANDIJON"},
    {label: "Ferghana", value: "FARGONA"},
    {label: "Namangan", value: "NAMANGAN"},
    {label: "Jizzakh", value: "JIZZAX"},
    {label: "Syrdarya", value: "SIRDARYO"},
    {label: "Samarkand", value: "SAMARQAND"},
    {label: "Navoi", value: "NANOIY"},
    {label: "Kashkadarya", value: "QASHQADARYO"},
    {label: "Surkhandarya", value: "SURXONDARYO"},
    {label: "Khorezm", value: "XORAZM"},
    {label: "Karakalpakstan", value: "QORAQALPOGISTON"},
]

const ChooseRegion = () => {
    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(null);
    const [username, setUsername] = useState('')
    const [items, setItems] = useState(regions);
    const [userImage, setUserImage] = useState('')

    const navigation = useNavigation()
    const user = useContext(UserContext)

    const changeUserData = async (uri, type, name) => {
        const formData = new FormData();
        formData.append('photo', {
            uri: uri,
            type: type,
            name: name,
        });
        try {
            const token = await AsyncStorage.getItem('token')
            const response = await axios.put(CHANGE_USER + user.userID, {
                    name: username,
                    role: user.role,
                    region: user.region,
                },
                {
                    headers: {
                        Authorization: `Token ${token}`
                    }
                })

            await AsyncStorage.setItem('phone', response.data?.phone)
            await AsyncStorage.setItem('userId', response.data?.id.toString())
            if (user.role === "client") {
                navigation.navigate('ClientHome')
            } else {
                navigation.navigate('Home')
            }
        } catch (error) {
            console.log(JSON.stringify(error.response, null, 4))
        }
    }

    const options = {
        mediaType: 'photo',
        multiple: false,
    };

    const openImagePicker = () => {
        ImagePicker.launchImageLibrary(options, async (response) => {
            if (response.didCancel) {
                console.log('Выбор изображения отменен');
            } else if (response.error) {
                console.log('Ошибка выбора изображения:', response.error);
            } else {
                for (let i of response['assets']) {
                    setUserImage(i.uri);
                }
            }
        });
    };

    return (
        <SafeAreaView className="flex-1 p-5 relative items-center">
            <Text className="text-[22px] font-semibold text-center mb-5">Personal information</Text>
            <View className="w-full">
                <Image className='w-28 h-28 rounded-full object-center mx-auto mb-5'
                       source={require('../../assets/user-default.jpeg')}/>
                <TouchableOpacity onPress={openImagePicker}>
                    <Text
                        className={`w-full border-0 px-3 py-4 rounded-xl text-[18px] mb-6 ${userImage ? 'bg-black/70 text-white' : 'bg-gray-200 text-gray-700'}`}>
                        {!userImage ? 'Upload image' : 'Image uploaded'}
                    </Text>
                </TouchableOpacity>
                <TextInput
                    className='w-full border-0 px-3 py-4 rounded-xl bg-gray-200 text-gray-700 text-[18px] mb-6'
                    onChangeText={setUsername}
                    value={username}
                    placeholder='Your name'
                />
                <DropDownPicker
                    placeholder="Select your region"
                    className='w-full border-0 px-3 py-4 rounded-xl bg-gray-200 text-gray-700'
                    dropDownContainerStyle={{
                        backgroundColor: "#E5E7EB",
                        borderWidth: 0
                    }}
                    textStyle={{fontSize: 18, fontWeight: '500', color: '#3d3c3c'}}
                    open={open}
                    value={value}
                    items={items}
                    setOpen={setOpen}
                    setValue={setValue}
                    setItems={setItems}
                    onChangeValue={(value) => user.region = value}
                />
            </View>
            <View className="absolute bottom-5 pb-5 w-full">
                <TouchableOpacity
                    disabled={!value}
                    onPress={changeUserData}
                    className={`w-full rounded-xl ${!value ? 'bg-black/50' : 'bg-black/70'}`}
                >
                    <Text className="text-[18px] font-semibold text-center text-white px-3 py-5 uppercase">
                        Continue
                    </Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    );
};

export default ChooseRegion;
