import React from 'react';
import {TouchableOpacity} from "react-native";
import {useNavigation} from "@react-navigation/native";
import {Path, Svg} from "react-native-svg";

const BackButton = () => {
    const navigation = useNavigation()

    return (
        <TouchableOpacity
            className="absolute z-50 top-5 left-5 rounded-full w-10 h-10 bg-white items-center justify-center"
            onPress={() => navigation.goBack()}
        >
            <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={24}
                height={24}
                viewBox="0 0 24 24"
                fill="none"
            >
                <Path
                    d="M15 19l-7-7 7-7"
                    stroke="#6B7280"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                />
            </Svg>
        </TouchableOpacity>
    );
};

export default BackButton;