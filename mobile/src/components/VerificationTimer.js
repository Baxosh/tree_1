import {Text} from "react-native";
import React, {useEffect} from "react";

const VerificationTimer = ({seconds, setSeconds}) => {
    useEffect(() => {
        if (seconds > 0) {
            setTimeout(() => setSeconds(seconds - 1), 1000);
        }
    });

    return (
        <Text className='text-[16px] text-gray-400'>Resend 00: {seconds < 10 ? '0' + seconds : seconds}</Text>
    )
}

export default VerificationTimer;
