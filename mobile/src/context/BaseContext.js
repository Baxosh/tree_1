import {createContext} from 'react';

const BaseContext = createContext('light');

const UserContext = createContext({
    userID: null,
    role: '',
    phone: '',
    region: '',
    token: ''
});

export {BaseContext, UserContext};