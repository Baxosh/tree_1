import {NavigationContainer} from "@react-navigation/native";
import {createNativeStackNavigator} from "@react-navigation/native-stack";
import Home from "./screens/Home";
import Login from "./screens/Login";
import VerificationCode from "./screens/VerificationCode";
import ChoiceRoles from "./screens/ChoiceRoles";
import ChooseRegion from "./screens/ChooseRegion";
import PlantRegion from "./screens/PlantRegion";
import PlantLocations from "./screens/PlantLocations";
import PlantLocationMap from "./screens/PlantLocationMap";
import ConfirmOrder from "./screens/ConfirmOrder";
import Transaction from "./screens/Transaction";
import ClientHome from "./screens/ClientHome";
import ClientMap from "./screens/ClientMap";
import ClientOrderLocations from "./screens/ClientOrderLocations";
import ClientOrderInfo from "./screens/ClientOrderInfo";
import Success from "./screens/Success";
import ClientOutMoney from "./screens/ClientOutMoney";
import ClientMyOrders from "./screens/ClientMyOrders";
import ClientCompleteOrder from "./screens/ClientCompleteOrder";
import Settings from "./screens/Settings";
import RatingOfDonators from "./screens/RatingOfDonators";
import Statistics from "./screens/Statistics";
import MyOrders from "./screens/MyOrders";
import ClientsRating from "./screens/ClientsRating";

const Stack = createNativeStackNavigator();

export default function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator
                screenOptions={{
                    headerShown: false,
                }}
            >
                <Stack.Screen name="Login" component={Login}/>
                <Stack.Screen name="Verification" component={VerificationCode}/>
                <Stack.Screen name="ChoiceRoles" component={ChoiceRoles}/>
                <Stack.Screen name="ChooseRegion" component={ChooseRegion}/>
                <Stack.Screen name="Home" component={Home}/>
                <Stack.Screen name="Settings" component={Settings}/>
                <Stack.Screen name="PlantRegion" component={PlantRegion}/>
                <Stack.Screen name="PlantLocation" component={PlantLocations}/>
                <Stack.Screen name="PlantLocationMap" component={PlantLocationMap}/>
                <Stack.Screen name="ConfirmOrder" component={ConfirmOrder}/>
                <Stack.Screen name="Transaction" component={Transaction}/>
                <Stack.Screen name="ClientHome" component={ClientHome}/>
                <Stack.Screen name="ClientMap" component={ClientMap}/>
                <Stack.Screen name="ClientOrderLocations" component={ClientOrderLocations}/>
                <Stack.Screen name="ClientOrderInfo" component={ClientOrderInfo}/>
                <Stack.Screen name="Success" component={Success}/>
                <Stack.Screen name="ClientOutMoney" component={ClientOutMoney}/>
                <Stack.Screen name="ClientMyOrders" component={ClientMyOrders}/>
                <Stack.Screen name="ClientCompleteOrder" component={ClientCompleteOrder}/>
                <Stack.Screen name="RatingOfDonators" component={RatingOfDonators}/>
                <Stack.Screen name="ClientsRating" component={ClientsRating}/>
                <Stack.Screen name="Statistics" component={Statistics}/>
                <Stack.Screen name="MyOrders" component={MyOrders}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
}
