export function getAverageOfTwoNumbers(obj) {
    let a = 0;
    let b = 0;
    let count = 0;

    for (const key in obj) {
        if (Array.isArray(obj[key]) && obj[key].length === 2) {
            const numericValues = obj[key].map(parseFloat);
            if (!isNaN(numericValues[0]) && !isNaN(numericValues[1])) {
                a += numericValues[0]
                b += numericValues[1]
                count++;
            }
        }
    }

    if (count === 0) {
        return 0;
    }

    return [a / count, b / count]
}

export const averageData = (data) => data.map(item => [item.id, ...getAverageOfTwoNumbers(item)])