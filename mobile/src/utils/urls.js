import {Platform} from "react-native";

export const domain = 'http://10.0.2.2:8000/api/v1'
//     Platform.select({
//     android: 'http://10.0.2.2:8000/api/v1',
//     ios: 'http://127.0.0.1:8000/api/v1'
// })

export const SEND_CODE = `${domain}/users/send-verification-code/`
export const SIGN_IN = `${domain}/users/sign-in/`
export const CHANGE_USER = `${domain}/users/user/`
export const PLANT = `${domain}/main/plant`
export const USER = `${domain}/users/user/`
export const DONATOR_RATING = `${domain}/main/donator`
export const DONATORS_ORDERS = `${domain}/main/donators/id/orders/`
