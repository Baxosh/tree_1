import React from 'react';
import {UserContext} from "./src/context/BaseContext";
import Router from "./src/Router";

const App = () => {
    const user = {
        role: '',
        phone: '',
        region: ''
    }

    return (
        <UserContext.Provider value={user}>
            <Router/>
        </UserContext.Provider>
    )
};

export default App