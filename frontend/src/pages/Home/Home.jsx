import React, {useContext, useEffect, useState} from 'react';
import './home.css'
import Layout from "../../components/Layout.jsx";
import {Context} from "../../contexts/BaseContext.jsx";
import Map from "../../components/Map.jsx";
import List from "../../components/List.jsx";
import {loadPlants} from "../../utils/loadPlants.js";
import {PLANTS} from "../../utils/urls.js";

function Home() {

    const {showMap, setAreas, setHelp, selected} = useContext(Context)

    useEffect(() => {
        loadPlants(PLANTS + `?region=${selected.filter}`, setAreas, setHelp)
    }, [selected]);

    return (<Layout>
        {showMap ? <Map/> : <List/>}
    </Layout>);
}

export default Home;