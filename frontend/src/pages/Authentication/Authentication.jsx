import React, {useContext, useEffect, useState} from 'react';
import PhoneInput from 'react-phone-number-input/input';
import './auth.css'
import {BiReset} from "react-icons/bi";
import {SEND_VERIFICATION_CODE, SIGN_IN} from "../../utils/urls.js";
import axios from "axios";
import {useNavigate} from "react-router-dom";
import {Context} from "../../contexts/BaseContext.jsx";
import Help from "../../components/Help.jsx";

function Authentication(props) {

    const {setHelp} = useContext(Context)
    const [phone, setPhone] = useState('')
    const [code, setCode] = useState('')
    const [timeLeft, setTimeLeft] = useState(60);
    const [timerRunning, setTimerRunning] = useState(false);
    const navigate = useNavigate()
    useEffect(() => {
        let timerInterval;
        if (timerRunning && timeLeft > 0) {
            timerInterval = setInterval(() => {
                setTimeLeft(prevTimeLeft => prevTimeLeft - 1);
            }, 1000);
        }
        return () => {
            clearInterval(timerInterval);
        };
    }, [timerRunning, timeLeft]);

    useEffect(() => setHelp(null), [phone])
    const signIn = e => {
        e.preventDefault()
        axios.post(SEND_VERIFICATION_CODE, {
            phone: phone.slice(1)
        }).then(res => {
            setTimeLeft(60)
            setTimerRunning(true)
            setHelp({
                status: 'success',
                text: "We send message!"
            })
            localStorage.setItem('user_id', res.data.user_id)
        }).catch(err => {
            setHelp({
                status: 'error',
                text: err.message
            })
            console.log(err)
        })
    }


    const confirm = e => {
        e.preventDefault()
        axios.post(SIGN_IN, {
            user_id: localStorage.getItem('user_id'),
            verification_code: code
        })
            .then(res => {
                localStorage.setItem('user', JSON.stringify(res.data.user))
                localStorage.setItem('token', res.data.token)
                localStorage.removeItem('user_id')
                navigate('/')
                setHelp({
                    status: 'success',
                    text: 'You successfully signed in'
                })
            })
            .catch(err => {
                setHelp({
                    status: 'error',
                    text: err.message
                })

                console.log(err)
            })

    }


    return (
        <div className="auth">
            <form className="auth__box" onSubmit={timerRunning ? confirm : signIn}>
                <h1>Номер телефона</h1>
                <PhoneInput
                    className="auth__input"
                    placeholder="Напишите номер телефона"
                    onChange={setPhone}
                    value={phone}
                />
                {timerRunning ? (
                    <>
                        <input
                            type="number"
                            className="auth__input auth__code"
                            placeholder="Потвердите смс код"
                            onChange={e => setCode(e.target.value)}
                            value={code}
                        />
                        {timeLeft > 0 ? (<>
                                <p className="auth__timer">{timeLeft}</p>
                            </>) :
                            <button
                                className="button auth__button"
                                onClick={signIn}
                            ><span>Отправит повторно</span> <BiReset size={20}/>
                            </button>
                        }
                    </>
                ) : ''}

                {timeLeft > 0 ? <button className="button auth__button">Подтвердить</button> : ""}
            </form>
        </div>
    );
}

export default Authentication;