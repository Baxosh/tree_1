import React from 'react';
import {createBrowserRouter, RouterProvider} from 'react-router-dom'
import Home from "./pages/Home/Home.jsx";
import Authentication from "./pages/Authentication/Authentication.jsx";
import AddTree from "./pages/AddTree/AddTree.jsx";
import Help from "./components/Help.jsx";

function Router() {

    const router = createBrowserRouter([
        {path: "/", element: <Home/>},
        {path: "auth", element: <Authentication/>},
        {path: 'add-tree', element: <AddTree/>},
    ]);

    return (
        <>
            <Help/>
            <RouterProvider router={router}/>
        </>
    );
}

export default Router;