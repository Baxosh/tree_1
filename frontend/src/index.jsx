import React from 'react'
import ReactDOM from 'react-dom/client'
import Router from './Router.jsx'
import './static/index.css'
import {BaseContext} from "./contexts/BaseContext.jsx";

ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <BaseContext>
            <Router/>
        </BaseContext>
    </React.StrictMode>,
)
