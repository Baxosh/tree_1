export const isAuthenticated = localStorage.getItem('user') && localStorage.getItem('token')
export const token = localStorage.getItem('token')
export const user = localStorage.getItem('user')