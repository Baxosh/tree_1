import axios from "axios";
import {token} from "./auth.js";


export const loadPlants = (url, setState, setHelp) => {
    axios.get(url, {
        headers: {
            Authorization: `Token ${token}`
        }
    })
        .then(res => setState(res.data))
        .catch(err => setHelp(
            {status: 'error', text: err.message}
        ))
}