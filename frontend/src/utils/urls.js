let BASE_URL = import.meta.env.VITE_BASE_URL + '/api/v1'
export let SEND_VERIFICATION_CODE = `${BASE_URL}/users/send-verification-code/`
export let SIGN_IN = `${BASE_URL}/users/sign-in/`
export let PLANTS = `${BASE_URL}/main/plant/`


