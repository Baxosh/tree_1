export const statusMaker = (isCompleted, isConfirmed) => {
    if (isCompleted && isConfirmed) {
        return {
            color: {
                color: '#00a100'
            },
            status: 'status_success'
        }
    } else if (!isCompleted && !isConfirmed) {
        return {
            color: {
                color: '#636363'
            },
            status: 'status_grey'
        }
    } else if (!isCompleted && isConfirmed) {
        return {
            color: {
                color: '#00a1d3'
            },
            status: 'status_info'
        }
    }
}