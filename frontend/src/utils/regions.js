export const regions = [{
    region: "Toshkent",
    coordinates: [41.2995, 69.2208],
    filter: 'TOSHKENT_VILOYAT'
}, {
    region: "Samarqand",
    coordinates: [39.6548, 66.9747],
    filter: 'SAMARQAND'
}, {
    region: "Buxoro",
    coordinates: [39.7750, 64.4259],
    filter: 'BUXORO'
}, {
    region: "Andijon",
    coordinates: [40.7821, 72.3185],
    filter: 'ANDIJON'
}, {
    region: "Namangan",
    coordinates: [40.9984, 71.6778],
    filter: 'NAMANGAN'
}, {
    region: "Farg'ona",
    coordinates: [40.3865, 71.7566],
    filter: 'FARGONA'
}, {
    region: "Navoiy",
    coordinates: [40.0457, 65.3537],
    filter: 'NANOIY'
}, {
    region: "Qoraqalpog'iston",
    coordinates: [42.9152, 60.6278],
    filter: 'QORAQALPOGISTON'
}, {
    region: "Xorazm",
    coordinates: [41.3797, 60.8596],
    filter: 'XORAZM'
}, {
    region: "Surxondaryo",
    coordinates: [37.1765, 67.7413],
    filter: 'SURXONDARYO'
}]