import React, {useContext} from 'react';
import ListItem from "./ListItem.jsx";
import {Context} from "../contexts/BaseContext.jsx";

function List() {
    const {areas} = useContext(Context)
    console.log(areas)
    return (
        <div className="list">
            {areas.length ? areas.map(area => (
                <ListItem key={area.id} area={area}/>
            )) : <h1 className="list__error">Malumot topilmadi</h1>}
        </div>
    );
}

export default List;