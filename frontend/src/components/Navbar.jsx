import React, {useContext} from 'react';
import {BsFillPlusCircleFill} from "react-icons/bs";
import {HiPlusCircle} from "react-icons/hi";
import {FaList, FaMapMarked} from "react-icons/fa";
import {Context} from "../contexts/BaseContext.jsx";
import {Link} from "react-router-dom";
import Regions from "./Regions.jsx";

function Navbar(props) {
    const {showMap, setShowMap} = useContext(Context)
    return (
        <nav className="navbar">
            <div className="navbar__start">
                <div className="navbar__item">
                    <div className="navbar__button-group">
                        <button
                            onClick={() => setShowMap(true)}
                            className={`button navbar__button ${showMap ? 'navbar__button_active' : ''}`}>
                            <span>Карта</span>
                            <FaMapMarked/>
                        </button>
                        <button
                            onClick={() => setShowMap(false)}
                            className={`button navbar__button ${!showMap ? 'navbar__button_active' : ''}`}>
                            <span>Список</span>
                            <FaList/>
                        </button>
                    </div>
                </div>
            </div>
            <div className="navbar__end">
                <div className="navbar__item">
                  <Regions/>

                </div>
                <Link to='/add-tree' className="navbar__item" >
                    <button className="button navbar__button"><span>Добавить</span> <HiPlusCircle size={20}/></button>
                </Link>
            </div>
        </nav>
    );
}

export default Navbar;