import React from 'react';
import Sidebar from "./Sidebar.jsx";
import Navbar from "./Navbar.jsx";
import {useLocation} from "react-router-dom";

function Layout({children}) {
    const location = useLocation()


    return (
        <div className="layout">
            <div className="layout__sidebar">
                <Sidebar/>
            </div>

            <div className="layout__content">
                {location.pathname === '/' ? <Navbar/> : ''}
                <div className="layout__section">
                    {children}
                </div>
            </div>
        </div>
    );
}

export default Layout;