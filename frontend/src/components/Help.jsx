import React, {useContext, useEffect} from 'react';
import {Context} from "../contexts/BaseContext.jsx";

function Help() {
    const {help, setHelp} = useContext(Context)

    useEffect(() => {
        const timer = setTimeout(() => {
            setHelp(null)
        }, 3000);
        return () => clearTimeout(timer);
    }, [help]);
    return (
        <>
            {help ? <p className={`help help_${help.status}`}>{help.text}</p> : ''}
        </>
    );
}

export default Help;