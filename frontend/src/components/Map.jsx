import React, {useContext, useEffect, useMemo, useState} from 'react';
import 'leaflet/dist/leaflet.css';
import '../pages/AddTree/addTree.css'
import {statusMaker} from "../utils/statusMaker.js";
import {
    MapContainer,
    Polygon,
    Popup,
    TileLayer,
} from "react-leaflet";
import {Context} from "../contexts/BaseContext.jsx";
import MapContent from "./MapContent.jsx";


function Map() {
    const {areas, mapZoom, selected} = useContext(Context)

    useEffect(() => {
        // location.reload()
    }, [selected])

    return (
        <div className="map">
            <div className="map__container">
                <MapContainer
                    center={selected.coordinates}
                    zoom={mapZoom} scrollWheelZoom={true} style={{width: '100%', height: '100%'}}>
                    <TileLayer
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        url="https://tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    {areas.length ? areas.map((area) => (<Polygon key={area.id} positions={[area.a, area.b, area.c, area.d]}
                                                                  pathOptions={statusMaker(area['is_completed'], area['is_confirmed']).color}>
                        <Popup>
                            <p>{area.address} </p>
                            <p>{area.planted}/{area.count}</p>
                        </Popup>
                    </Polygon>)) : ''}
                </MapContainer>
            </div>
            <MapContent/>
        </div>
    );
}

export default Map;