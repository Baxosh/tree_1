import React, {useEffect, useState} from 'react';
import 'leaflet/dist/leaflet.css';
import 'leaflet-draw/dist/leaflet.draw.css'
import {MapContainer, TileLayer, FeatureGroup} from "react-leaflet";
import {EditControl} from 'react-leaflet-draw'
import {Link} from "react-router-dom";

const center = [39.770883, 64.444050]


function AddTreeMap() {
    const [mapLayers, setMapLayers] = useState([])
    const [coordinates, setCoordinates] = useState([])
    const [status, setStatus] = useState("green")

    const _created = e => {
        const {layerType, layer} = e
        if (layerType === 'polygon') {
            const {_leaflet_id} = layer
            setMapLayers(layers => [...layers, {id: _leaflet_id, latlngs: layer.getLatLngs()[0],}])
        }
    }

    const _edited = (e) => {
        console.log(e);
        const {
            layers: {_layers},
        } = e;

        Object.values(_layers).map(({_leaflet_id, editing}) => {
            setMapLayers((layers) =>
                layers.map((l) =>
                    l.id === _leaflet_id
                        ? {...l, latlngs: {...editing.latlngs[0]}}
                        : l
                )
            );
        });
    };

    const _deleted = e => {
        const {layers: {_layers}} = e

        Object.values(_layers).map(({_leaflet_id}) => {
            setMapLayers(layers => layers.filter(l => l.id !== _leaflet_id))

        })
    }

    useEffect(() => {
        mapLayers.length && setCoordinates(mapLayers[0].latlngs.map(item => {
            return [item.lat, item.lng]
        }))

    }, [mapLayers])

    console.log({
        a: coordinates[0],
        b: coordinates[1],
        c: coordinates[2],
        d: coordinates[3],

    })

    return (
        <div className="map">
            <div className="map__container_add">
                <Link to='/' className='addTree_beck'>Назад</Link>
                <MapContainer center={center} zoom={13} scrollWheelZoom={true} style={{width: '100%', height: '100%'}}>
                    <FeatureGroup>
                        <EditControl onCreated={_created} onEdited={_edited} onDeleted={_deleted} draw={{
                            circle: false,
                            circlemarker: false,
                            polygon: {showArea: false, shapeOptions: {color: status}},
                            rectangle: false,
                            polyline: false,
                            marker: false,
                        }}/>
                    </FeatureGroup>
                    <TileLayer
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        url="https://tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />

                </MapContainer>
                <form method='post' className="box-status">
                    <label htmlFor="scales">Scales</label>
                    <input required pattern="[0-9]" type="number"/>
                    <button>Отправить</button>
                </form>
            </div>
            {/*<button onClick={() => setStatus('green')}>Green</button>*/}
            {/*<button onClick={() => setStatus('grey')}>Grey</button>*/}
        </div>
    );
}

export default AddTreeMap;

