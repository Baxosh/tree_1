import React from 'react';
import {BsFillPinMapFill} from "react-icons/bs";
import {BiSolidLogInCircle} from "react-icons/bi";
import {Link, useNavigate} from "react-router-dom";
import {RiMoneyDollarCircleFill} from "react-icons/ri";

function Sidebar() {
    const navigate = useNavigate()
    const logOut = () => {
        localStorage.removeItem('user')
        localStorage.removeItem('token')
        navigate('/auth')

    }

    return (
        <div className="sidebar">
            <div className="sidebar__top">
                <div className="sidebar__account">
                    <div className="sidebar__avatar">
                        <img
                            src="https://st3.depositphotos.com/6672868/13701/v/450/depositphotos_137014128-stock-illustration-user-profile-icon.jpg"
                            alt=""
                        />
                    </div>
                    <h1 className="sidebar__username">Shakhboz Vohidov</h1>
                </div>
                <div className="sidebar__menu">
                    <Link className="sidebar__item" to={'/'}><span>Hududlar</span> <BsFillPinMapFill/> </Link>
                    <a className="sidebar__item"><span>Aktiv buyurtmalar</span> <BsFillPinMapFill/> </a>
                    <a className="sidebar__item"><span>Murojaatlar</span> <RiMoneyDollarCircleFill/> </a>
                </div>
            </div>
            <div className="sidebar__bottom">
                <div className="sidebar__menu">
                    <a className="sidebar__item" onClick={logOut}><span>Chiqish</span> <BiSolidLogInCircle/> </a>
                </div>
            </div>
        </div>
    );
}

export default Sidebar;