import React, {useContext} from 'react';
import {BiSolidMessageSquareEdit} from "react-icons/bi";
import {statusMaker} from "../utils/statusMaker.js";
import {ImLocation2} from "react-icons/im";
import {Context} from "../contexts/BaseContext.jsx";
import {useNavigate} from "react-router-dom";

function ListItem({area}) {
    const {setMapCenter, setMapZoom, setShowMap} = useContext(Context)
    const navigate = useNavigate()


    const navigateToArea = (center) => {
        setShowMap(true)
        setMapCenter(center)
        setMapZoom(20)
    }


    return (
        <div className="list__item">
            <div className="list__content">
                <ImLocation2 size={40} color={'crimson'} onClick={()=> navigateToArea(area.a)}/>
                <h1>Buxoro shahar, Bahouddin naqshband ko`chasi</h1>
            </div>
            <div className="list__status">
                <h1>{area.planted}/{area.count}</h1>
                <div className={`status ${statusMaker(area.is_completed, area.is_confirmed).status}`}>
                </div>
                {/*<button className="button list__button"><span>Изменить</span> <BiSolidMessageSquareEdit size={18}/>*/}
                {/*</button>*/}
            </div>
        </div>);
}

export default ListItem;