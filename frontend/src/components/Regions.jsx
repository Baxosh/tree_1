import React, {useContext, useState} from 'react';
import {MdArrowDropDownCircle} from "react-icons/md";
import {Context} from "../contexts/BaseContext.jsx";
import {useNavigate} from "react-router-dom";
import {regions} from "../utils/regions.js";


function Regions(props) {
    const navigate = useNavigate()
    const {selected, setSelected, setMapZoom} = useContext(Context)
    const [active, setActive] = useState(false)


    const filter = (item) => {
        setSelected(item)
        setActive(!active)
        setMapZoom(20)
        location.reload()
    }

    return (
        <div className="dropdown">
            <div className="dropdown-trigger">
                <button
                    className="button navbar__button"
                    aria-haspopup="true"
                    aria-controls="dropdown-menu3"
                    onClick={() => setActive(!active)}
                >
                    <span>{selected.region}</span>
                    <span className="icon is-small"><MdArrowDropDownCircle/></span>
                </button>
            </div>
            <div className={`dropdown-menu ${active ? 'active' : ''}`}>
                <div className="dropdown-content">
                    {regions.map(item => (<div
                        className="dropdown-item"
                        key={item.coordinates}
                        onClick={() => filter(item)}
                    >{item.region}</div>))}
                </div>
            </div>
        </div>


    );
}

export default Regions;