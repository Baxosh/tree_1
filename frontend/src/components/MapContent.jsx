import React, {useContext} from 'react';
import {Context} from "../contexts/BaseContext.jsx";
import {statusMaker} from "../utils/statusMaker.js";

function MapContent() {
    const {areas} = useContext(Context)
    const status_info = areas.filter(area => statusMaker(area.is_completed, area.is_confirmed).status === 'status_info')
    const status_grey = areas.filter(area => statusMaker(area.is_completed, area.is_confirmed).status === 'status_grey')
    const status_success = areas.filter(area => statusMaker(area.is_completed, area.is_confirmed).status === 'status_success')

    return (
        <div className="map__content">
            <div className="map__status">
                <div className="status status_info"></div>
                <span>Можно – {status_info.length}</span>
            </div>
            <div className="map__status">
                <div className="status status_success"></div>
                <span>Заполнено – {status_success.length}</span>
            </div>
            <div className="map__status">
                <div className="status status_grey"></div>
                <span>Неизвестно – {status_grey.length}</span>
            </div>


        </div>
    );
}

export default MapContent;