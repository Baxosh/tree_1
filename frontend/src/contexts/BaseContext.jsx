import React, {createContext, useEffect, useState} from 'react'

export const Context = createContext({})

export function BaseContext({children}) {
    const [showMap, setShowMap] = useState(JSON.parse(localStorage.getItem('showMap')) || false)
    const [help, setHelp] = useState(null)
    const [areas, setAreas] = useState([])
    const [mapZoom, setMapZoom] = useState(13)
    const [selected, setSelected] = useState(JSON.parse(localStorage.getItem('selected')) || {
        region: "Buxoro",
        coordinates: [39.7750, 64.4259],
        filter: 'BUXORO',
    })

    useEffect(() => localStorage.setItem('selected', JSON.stringify(selected)))
    useEffect(() => {
        localStorage.setItem("showMap", JSON.stringify(showMap))
    }, [showMap]);


    return (
        <Context.Provider
            value={{
                showMap, setShowMap, help,
                setHelp, areas, setAreas,
                mapZoom, setMapZoom, selected, setSelected
            }}>
            {children}
        </Context.Provider>
    )
}